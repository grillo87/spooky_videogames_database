<b>Spooky VideoGames DB</b>

<img src="https://gitlab.com/grillo87/spooky_videogames_database/raw/master/app/src/main/ic_launcher-web.png" alt="" width="240" height="240">

<body>
<b>Spooky VideoGames DB developed Kotlin for Android</b></br></br>

This is a an app developed using Kotlin for Android.It uses Model View View Model (MVVM) as architectural pattern.</br>
Also all the layouts were developed using Constrains Layouts.</br>
For the web service area, it was integrated with IGDB.com API, this API is consumed with Retrofit2 and using RXAndroid.</br>
Also the app includes dependency injections and Dagger2 was used for that part.</br>
The project includes Unit Testing for web service area and main functions, using mock server and Robolectric</br>

The information displayed of the games comes from the mentionated API.</br>

The app is available on Google Play on:</br></br>

<a href="https://play.google.com/store/apps/details?id=com.josegrillo.spookyvideogamesdatabase"><img src="https://cdn-images-1.medium.com/max/1920/1*OIIv4FEjJQMqh-zEPhtlYA.png" title="Google Play Link" alt="Google" width="231" height="80"></a>

The app uses the following libraries:</br></br>

- <a href="https://github.com/google/dagger">Dagger2</a></br>
- <a href="https://github.com/pnikosis/materialish-progress">Material-ish Progress</a></br>
- <a href="https://github.com/square/retrofit">Retrofit2</a></br></br>
- <a href="https://github.com/ReactiveX/RxJava">RxJava: Reactive Extensions for the JVM</a></br></br>
- <a href="https://github.com/ReactiveX/RxAndroid">RxAndroid: Reactive Extensions for Android</a></br></br>
- <a href="https://github.com/ihsanbal/LoggingInterceptor">LoggingInterceptor - Interceptor for OkHttp3 with pretty logger</a></br></br>
- <a href="https://github.com/bumptech/glide">Glide</a></br></br>
- <a href="https://developers.google.com/admob/android/quick-start">Google Admob</a></br></br>
- <a href="http://robolectric.org/">Robolectric</a></br></br>


As reference for the definition of the architecture of the project the following resources were used as reference:</br></br>

- <a href="https://medium.com/@manuelvicnt/rxjava-android-mvvm-app-structure-with-retrofit-a5605fa32c00">RxJava: Android MVVM App structure with Retrofit</a></br>
- <a href="https://medium.com/capital-one-developers/rxjava2-android-mvvm-lifecycle-app-structure-with-retrofit-2-cf903849f49e">RxJava 2: Android MVVM Lifecycle App Structure with Retrofit 2</a></br></br>
- <a href="https://github.com/wasabeef/kotlin-mvvm">Sample for MVVM using Kotlin</a></br></br>
- <a href="https://medium.com/@nicolas.duponchel/testing-viewmodel-in-mvvm-using-livedata-and-rxjava-b27878495220">Testing ViewModel in MVVM using LiveData and RxJava</a></br>
- <a href="https://github.com/WellingtonCosta/android-mvvm-databinding-kotlin">A simple Android application project using MVVM + Data Binding + Retrofit + RxJava and written in Kotlin.</a></br></br>

You could find me at LinkedIn on:</br></b>

<a href="https://www.linkedin.com/in/jos%C3%A9-enrique-grillo-hern%C3%A1ndez-4955645a/?locale=en_US">LinkedIn Profile</a></br></br>

<b>Screenshots</b></br></br>
<table>
<tr>
<td>
<img src="https://gitlab.com/grillo87/spooky_videogames_database/raw/master/screenshots/Screenshot_1537398478.png">
</td>
<td>
<img src="https://gitlab.com/grillo87/spooky_videogames_database/raw/master/screenshots/Screenshot_1537398492.png">
</td>
<td>
<img src="https://gitlab.com/grillo87/spooky_videogames_database/raw/master/screenshots/Screenshot_1537398508.png">
</td>
<td>
<img src="https://gitlab.com/grillo87/spooky_videogames_database/raw/master/screenshots/Screenshot_1537398533.png">
</td>
</tr>
</table>


</body>
</html>