package com.josegrillo.kotlinmvp.utils

class AppConstants {

    companion object {

        val BASE_URL = "https://api-endpoint.igdb.com/"

        val HEADER_ACCEPT_TAG = "Accept"
        val HEADER_APPLICATION_JSON_TAG = "application/json"
        val USER_KEY_TAG = "user-key"
        val USER_KEY_VALUE = "IGDB-KEY"
        val REQUEST_TAG = "Request"
        val RESPONSE_TAG = "Response"

    }

}