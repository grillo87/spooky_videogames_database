package com.josegrillo.spookyvideogamesdatabase.domain.usecase

import com.josegrillo.kotlinmvp.data.remote.AppApi
import com.josegrillo.spookyvideogamesdatabase.domain.model.api.request.RequestFields
import com.josegrillo.spookyvideogamesdatabase.domain.model.api.response.Developer
import io.reactivex.Observable
import javax.inject.Inject

class GetCompaniesById @Inject constructor(private val appApi: AppApi) {

    fun getCompaniesById(ids: String): Observable<List<Developer>> = appApi.companiesById(ids,
            RequestFields.NAME_AND_LOGO_FIELDS_VALUES)

}