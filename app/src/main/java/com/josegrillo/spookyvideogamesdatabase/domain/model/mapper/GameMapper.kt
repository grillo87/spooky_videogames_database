package com.josegrillo.spookyvideogamesdatabase.domain.model.mapper

import com.josegrillo.spookyvideogamesdatabase.view.model.Game
import com.josegrillo.spookyvideogamesdatabase.view.model.Image
import com.josegrillo.spookyvideogamesdatabase.view.utils.StringUtils

class GameMapper {

    companion object {

        fun gameApiResultToViewModel(gameApi: com.josegrillo.spookyvideogamesdatabase.domain.model.api.response.Game): Game {

            return Game(idServer = gameApi.id,
                    name = gameApi.name,
                    summary = gameApi.summary,
                    rating = gameApi.totalRating,
                    esrbRating = EsrbMapper.esrbApiToViewModelRating(gameApi.esrb),
                    releaseDate = StringUtils.formatDateGame(gameApi.releaseDate, StringUtils.DATE_FORMAT),
                    cover = Image(StringUtils.formatImageUrl(gameApi.cover?.url, StringUtils.COVER_BIG_VALUE)))

        }

        fun gamesApiResultToGamesViewModel(gamesApi: List<com.josegrillo.spookyvideogamesdatabase.domain.model.api.response.Game>): ArrayList<Game> {

            val result = ArrayList<Game>()

            gamesApi.forEach {

                result.add(gameApiResultToViewModel(it))

            }

            return result

        }

    }

}