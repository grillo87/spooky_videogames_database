package com.josegrillo.spookyvideogamesdatabase.domain.model.mapper

import com.josegrillo.spookyvideogamesdatabase.domain.model.api.response.PlayerPerspective
import com.josegrillo.spookyvideogamesdatabase.view.model.PlayersPerspective

class PlayerPerspectiveMapper {

    companion object {

        fun playerPerspectiveApiListToVieModelList(playersPerspectives: List<PlayerPerspective>): ArrayList<PlayersPerspective> {

            val result = ArrayList<PlayersPerspective>()

            playersPerspectives.forEach {

                result.add(playerPerspectiveApiToViewModel(it))

            }

            return result

        }

        fun playerPerspectiveApiToViewModel(playerPerspective: PlayerPerspective): PlayersPerspective = PlayersPerspective(playerPerspective.name)

    }

}