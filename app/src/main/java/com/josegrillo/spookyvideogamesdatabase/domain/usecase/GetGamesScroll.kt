package com.josegrillo.spookyvideogamesdatabase.domain.usecase

import com.josegrillo.kotlinmvp.data.remote.AppApi
import com.josegrillo.spookyvideogamesdatabase.domain.model.api.request.RequestFields
import com.josegrillo.spookyvideogamesdatabase.domain.model.api.response.Game
import io.reactivex.Observable
import javax.inject.Inject

class GetGamesScroll @Inject constructor(private val appApi: AppApi) {

    fun getGameScroll(scroll: String): Observable<List<Game>> = appApi.gameScroll(scroll)

}