package com.josegrillo.spookyvideogamesdatabase.domain.model.api.response

data class Image(val url: String)