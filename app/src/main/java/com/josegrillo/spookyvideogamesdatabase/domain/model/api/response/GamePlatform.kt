package com.josegrillo.spookyvideogamesdatabase.domain.model.api.response

data class GamePlatform(val id: Long,
                        val name: String,
                        val logo: Image? = null)