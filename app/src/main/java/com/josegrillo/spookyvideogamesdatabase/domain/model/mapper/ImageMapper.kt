package com.josegrillo.spookyvideogamesdatabase.domain.model.mapper

import com.josegrillo.spookyvideogamesdatabase.view.model.Image
import com.josegrillo.spookyvideogamesdatabase.view.utils.StringUtils

class ImageMapper {

    companion object {


        fun imageApiResultToImageViewModel(image: com.josegrillo.spookyvideogamesdatabase.domain.model.api.response.Image): Image = Image(StringUtils.formatImageUrl(image.url, StringUtils.COVER_BIG_VALUE))

        fun imagesApiToImagesViewModel(imagesList: List<com.josegrillo.spookyvideogamesdatabase.domain.model.api.response.Image>?): ArrayList<Image> {

            val result = ArrayList<Image>()

            imagesList?.let {

                it.forEach {

                    result.add(imageApiResultToImageViewModel(it))

                }

            }

            return result

        }

    }

}