package com.josegrillo.spookyvideogamesdatabase.domain.model.api.response

data class GameCollection(val id: Long,
                          val games: ArrayList<Long>? = ArrayList())