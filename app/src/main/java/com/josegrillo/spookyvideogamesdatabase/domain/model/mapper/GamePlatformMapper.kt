package com.josegrillo.spookyvideogamesdatabase.domain.model.mapper

import com.josegrillo.spookyvideogamesdatabase.domain.model.api.response.GamePlatform
import com.josegrillo.spookyvideogamesdatabase.view.model.Image
import com.josegrillo.spookyvideogamesdatabase.view.utils.StringUtils

class GamePlatformMapper {

    companion object {

        fun gamePlatformApiToGamePlatformViewModel(gamePlatform: GamePlatform): com.josegrillo.spookyvideogamesdatabase.view.model.GamePlatform =
                com.josegrillo.spookyvideogamesdatabase.view.model.GamePlatform(gamePlatform.name,
                        gamePlatform.id,
                        Image(StringUtils.formatImageUrl(gamePlatform.logo?.url, StringUtils.LOGO_MED_VALUE)))


        fun gamePlatformsApiToGamePlatformsViewModel(gamePlatforms: List<GamePlatform>): ArrayList<com.josegrillo.spookyvideogamesdatabase.view.model.GamePlatform> {

            val result = ArrayList<com.josegrillo.spookyvideogamesdatabase.view.model.GamePlatform>()

            gamePlatforms.forEach {

                result.add(gamePlatformApiToGamePlatformViewModel(it))

            }

            return result

        }


    }


}