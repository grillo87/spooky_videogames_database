package com.josegrillo.spookyvideogamesdatabase.domain.model.api.request

class RequestFields {

    companion object {


        val DEFAULT_FIELDS_VALUES = "name,summary,storyline,collection,total_rating,developers,time_to_beat,player_perspectives,status,first_release_date,screenshots,cover,esrb,artworks"
        val NAME_AND_LOGO_FIELDS_VALUES = "name,logo"
        val NAME_AND_COVER_FIELDS_VALUES = "name,cover"
        val GAMES_FIELDS_VALUE = "games"
        val NAME_FIELD_VALUE = "name"


        val DEFAULT_LIMIT_VALUE = 10
        val DEFAULT_SCROLL_VALUE = 1
        val DEFAUL_GENRE_VALUE = 31
        val DEFAULT_THEME_VALUE = 19
        val DEFAULT_POPULARITY_VALUE = "popularity:desc"
        val DEFAULT_NAME_VALUE = "name:asc"

        val GAME_NAMES_FILTER_VALUE = "dead space,resident evil,silent hill,evil within,alone in the dark,detention,until dawn,alan wake,deadly premonition"


    }

}