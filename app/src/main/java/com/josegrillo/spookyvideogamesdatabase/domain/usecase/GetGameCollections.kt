package com.josegrillo.spookyvideogamesdatabase.domain.usecase

import com.josegrillo.kotlinmvp.data.remote.AppApi
import com.josegrillo.spookyvideogamesdatabase.domain.model.api.request.RequestFields
import com.josegrillo.spookyvideogamesdatabase.domain.model.api.response.Game
import com.josegrillo.spookyvideogamesdatabase.domain.model.api.response.GameCollection
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class GetGameCollections @Inject constructor(private val appApi: AppApi) {

    fun getGameCollections(ids: String): Observable<List<GameCollection>> = appApi.gameCollections(ids,
            RequestFields.GAMES_FIELDS_VALUE, RequestFields.DEFAULT_LIMIT_VALUE)

}