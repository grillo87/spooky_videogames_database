package com.josegrillo.spookyvideogamesdatabase.domain.usecase

import com.josegrillo.kotlinmvp.data.remote.AppApi
import com.josegrillo.spookyvideogamesdatabase.domain.model.api.request.RequestFields
import com.josegrillo.spookyvideogamesdatabase.domain.model.api.response.PlayerPerspective
import io.reactivex.Observable
import javax.inject.Inject

class GetPlayersPerspectives @Inject constructor(private val appApi: AppApi) {

    fun getPlayersPerspective(ids: String): Observable<List<PlayerPerspective>> = appApi.playersPerspective(ids,
            RequestFields.NAME_FIELD_VALUE)

}