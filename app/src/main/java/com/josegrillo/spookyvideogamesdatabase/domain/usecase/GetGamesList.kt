package com.josegrillo.spookyvideogamesdatabase.domain.usecase

import com.josegrillo.kotlinmvp.data.remote.AppApi
import com.josegrillo.spookyvideogamesdatabase.domain.model.api.request.RequestFields
import com.josegrillo.spookyvideogamesdatabase.domain.model.api.response.Game
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Response
import javax.inject.Inject

class GetGamesList @Inject constructor(private val appApi: AppApi) {

    fun getGamesList(): Observable<Response<List<Game>>> = appApi.gamesList(RequestFields.NAME_AND_COVER_FIELDS_VALUES,
            RequestFields.DEFAULT_LIMIT_VALUE, RequestFields.DEFAULT_SCROLL_VALUE,
            RequestFields.GAME_NAMES_FILTER_VALUE,
            RequestFields.DEFAULT_POPULARITY_VALUE)

}