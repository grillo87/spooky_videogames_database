package com.josegrillo.spookyvideogamesdatabase.domain.model.api.response

import com.google.gson.annotations.SerializedName

data class Game(val id: Long,
                val name: String,
                val summary: String? = null,
                val collection: Long? = null,
                @SerializedName("total_rating") val totalRating: Double? = null,
                val developers: ArrayList<Long>? = null,
                @SerializedName("time_to_beat") val timeToBeat: TimeToBeat? = null,
                @SerializedName("player_perspectives") val playerPerspectives: ArrayList<Long>? = null,
                @SerializedName("first_release_date") val releaseDate: Long? = null,
                val platforms: ArrayList<Long>? = null,
                val games: ArrayList<Long>? = null,
                val screenshots: ArrayList<Image>? = null,
                val cover: Image? = null,
                val esrb: Esrb? = null,
                val artworks: ArrayList<Image>? = null) {


}