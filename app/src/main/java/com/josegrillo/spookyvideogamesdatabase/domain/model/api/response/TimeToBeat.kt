package com.josegrillo.spookyvideogamesdatabase.domain.model.api.response

data class TimeToBeat(val hastly: Long? = null,
                      val normally: Long? = null,
                      val completely: Long? = null)