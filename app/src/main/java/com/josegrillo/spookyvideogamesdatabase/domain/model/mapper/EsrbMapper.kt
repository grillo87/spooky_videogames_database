package com.josegrillo.spookyvideogamesdatabase.domain.model.mapper

import com.josegrillo.spookyvideogamesdatabase.domain.model.api.response.Esrb
import com.josegrillo.spookyvideogamesdatabase.view.model.GameRating

class EsrbMapper {

    companion object {

        fun esrbApiToViewModelRating(esrb: Esrb?): GameRating {

            var result = GameRating.RP

            if (esrb != null) {

                result = GameRating.from(esrb.rating)

            }

            return result

        }


    }


}