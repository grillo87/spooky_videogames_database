package com.josegrillo.spookyvideogamesdatabase.domain.model.api.response

data class PlayerPerspective(val id: Long,
                             val name: String)