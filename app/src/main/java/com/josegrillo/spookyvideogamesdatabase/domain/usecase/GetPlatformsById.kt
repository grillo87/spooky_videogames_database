package com.josegrillo.spookyvideogamesdatabase.domain.usecase

import com.josegrillo.kotlinmvp.data.remote.AppApi
import com.josegrillo.spookyvideogamesdatabase.domain.model.api.request.RequestFields
import com.josegrillo.spookyvideogamesdatabase.domain.model.api.response.GamePlatform
import io.reactivex.Observable
import javax.inject.Inject

class GetPlatformsById @Inject constructor(private val appApi: AppApi) {

    fun getGamePlatforms(ids: String): Observable<List<GamePlatform>> = appApi.platforms(ids,
            RequestFields.NAME_AND_LOGO_FIELDS_VALUES)

}