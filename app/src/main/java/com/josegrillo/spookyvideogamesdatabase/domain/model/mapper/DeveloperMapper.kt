package com.josegrillo.spookyvideogamesdatabase.domain.model.mapper

import com.josegrillo.spookyvideogamesdatabase.domain.model.api.response.Developer
import com.josegrillo.spookyvideogamesdatabase.domain.model.api.response.PlayerPerspective
import com.josegrillo.spookyvideogamesdatabase.view.model.Image
import com.josegrillo.spookyvideogamesdatabase.view.model.PlayersPerspective
import com.josegrillo.spookyvideogamesdatabase.view.utils.StringUtils

class DeveloperMapper {

    companion object {

        fun developersApiToDevelopersViewModel(developers: List<Developer>): ArrayList<com.josegrillo.spookyvideogamesdatabase.view.model.Developer> {

            val result = ArrayList<com.josegrillo.spookyvideogamesdatabase.view.model.Developer>()

            developers.forEach {

                result.add(developerApiToDeveloperViewModel(it))

            }

            return result

        }

        fun developerApiToDeveloperViewModel(developer: Developer): com.josegrillo.spookyvideogamesdatabase.view.model.Developer = com.josegrillo.spookyvideogamesdatabase.view.model.Developer(developer.name,
                Image(StringUtils.formatImageUrl(developer.logo?.url, StringUtils.LOGO_MED_VALUE)))


    }

}