package com.josegrillo.spookyvideogamesdatabase.domain.usecase

import com.josegrillo.kotlinmvp.data.remote.AppApi
import com.josegrillo.spookyvideogamesdatabase.domain.model.api.response.Game
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class GetGamesById @Inject constructor(private val appApi: AppApi) {

    fun getGamesById(ids: String): Observable<List<Game>> = appApi.gameById(ids)

}