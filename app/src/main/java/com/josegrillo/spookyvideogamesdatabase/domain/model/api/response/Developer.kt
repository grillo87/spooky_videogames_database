package com.josegrillo.spookyvideogamesdatabase.domain.model.api.response

data class Developer(val id: Long,
                     val name: String,
                     val logo: Image? = null)