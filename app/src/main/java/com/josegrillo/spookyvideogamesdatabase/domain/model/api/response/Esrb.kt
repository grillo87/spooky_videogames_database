package com.josegrillo.spookyvideogamesdatabase.domain.model.api.response

data class Esrb(val rating: Long,
                val synopsis: String? = null)