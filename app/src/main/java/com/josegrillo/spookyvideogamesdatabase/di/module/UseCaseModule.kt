package com.josegrillo.kotlinmvp.di.module

import com.josegrillo.kotlinmvp.data.remote.AppApi
import com.josegrillo.spookyvideogamesdatabase.domain.usecase.*
import dagger.Module
import dagger.Provides

@Module
class UseCaseModule {


    @Provides
    fun providesGetCompaniesById(appApi: AppApi): GetCompaniesById {
        return GetCompaniesById(appApi)
    }

    @Provides
    fun providesGetGamesById(appApi: AppApi): GetGamesById {
        return GetGamesById(appApi)
    }

    @Provides
    fun providesGetGamesList(appApi: AppApi): GetGamesList {
        return GetGamesList(appApi)
    }

    @Provides
    fun providesGetGamesScroll(appApi: AppApi): GetGamesScroll {
        return GetGamesScroll(appApi)
    }

    @Provides
    fun providesGetPlatformById(appApi: AppApi): GetPlatformsById {
        return GetPlatformsById(appApi)
    }

    @Provides
    fun providesGetPlayersPerspectives(appApi: AppApi): GetPlayersPerspectives {
        return GetPlayersPerspectives(appApi)
    }

    @Provides
    fun providesGameCollections(appApi: AppApi): GetGameCollections {
        return GetGameCollections(appApi)
    }


}