package com.josegrillo.spookyvideogamesdatabase.di.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.josegrillo.spookyvideogamesdatabase.base.ViewModelFactory
import com.josegrillo.spookyvideogamesdatabase.base.ViewModelKey
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewModel.*
import com.josegrillo.spookyvideogamesdatabase.view.ui.main.fragment.list.viewmodel.ListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    internal abstract fun provideInternalDetailViewModel(viewModel: DetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PlayerPerspectivesViewModel::class)
    internal abstract fun provideInternalPlayerPerspectiveViewModel(viewModel: PlayerPerspectivesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DevelopersViewModel::class)
    internal abstract fun provideInternalDevelopersViewModel(viewModel: DevelopersViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PlatformViewModel::class)
    internal abstract fun provideInternalPlatformsViewModel(viewModel: PlatformViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RelatedGamesViewModel::class)
    internal abstract fun provideInternalRelatedGamesViewModel(viewModel: RelatedGamesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ListViewModel::class)
    internal abstract fun provideInternalListGamesViewModel(viewModel: ListViewModel): ViewModel
}