package com.josegrillo.spookyvideogamesdatabase.di.module

import com.josegrillo.spookyvideogamesdatabase.domain.usecase.*
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewModel.DevelopersViewModel
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewModel.PlatformViewModel
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewModel.PlayerPerspectivesViewModel
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewModel.RelatedGamesViewModel
import com.josegrillo.spookyvideogamesdatabase.view.ui.main.fragment.list.viewmodel.ListViewModel
import dagger.Module
import dagger.Provides

@Module
class FragmentsModule {

    @Provides
    fun providePlayersPerspectiveViewModel(getPlayersPerspective: GetPlayersPerspectives): PlayerPerspectivesViewModel {

        return PlayerPerspectivesViewModel(getPlayersPerspective)

    }

    @Provides
    fun provideDevelopersViewModel(getCompaniesById: GetCompaniesById): DevelopersViewModel {

        return DevelopersViewModel(getCompaniesById)

    }

    @Provides
    fun providePlatformViewModel(getPlatformsById: GetPlatformsById): PlatformViewModel {

        return PlatformViewModel(getPlatformsById)

    }

    @Provides
    fun provideRelatedGamesViewModel(getGameCollections: GetGameCollections, getGamesById: GetGamesById): RelatedGamesViewModel {

        return RelatedGamesViewModel(getGameCollections, getGamesById)

    }


    @Provides
    fun provideListGamesViewModel(getGamesList: GetGamesList, getGamesScroll: GetGamesScroll): ListViewModel {

        return ListViewModel(getGamesList, getGamesScroll)

    }

}