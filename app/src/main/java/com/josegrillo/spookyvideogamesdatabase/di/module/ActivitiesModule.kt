package com.josegrillo.kotlinmvp.di.module


import com.josegrillo.spookyvideogamesdatabase.domain.usecase.GetCompaniesById
import com.josegrillo.spookyvideogamesdatabase.domain.usecase.GetGamesById
import com.josegrillo.spookyvideogamesdatabase.domain.usecase.GetPlayersPerspectives
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewModel.DetailViewModel
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewModel.PlayerPerspectivesViewModel
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@Module
class ActivitiesModule {

    @Provides
    fun provideSubscription(): CompositeDisposable = CompositeDisposable()

    @Provides
    fun provideDetailViewModel(getGamesById: GetGamesById): DetailViewModel {

        return DetailViewModel(getGamesById)

    }


}