package com.josegrillo.kotlinmvp.di.component

import com.josegrillo.kotlinmvp.di.module.ActivitiesModule
import com.josegrillo.kotlinmvp.di.module.RemoteRepositoryModule
import com.josegrillo.kotlinmvp.di.module.UseCaseModule
import com.josegrillo.spookyvideogamesdatabase.di.module.FragmentsModule
import com.josegrillo.spookyvideogamesdatabase.di.module.ViewModelModule
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.activity.DetailActivity
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.fragment.*
import com.josegrillo.spookyvideogamesdatabase.view.ui.main.activity.MainActivity
import com.josegrillo.spookyvideogamesdatabase.view.ui.main.fragment.list.fragment.ListFragment
import com.josegrillo.spookyvideogamesdatabase.view.ui.splash.SplashActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ActivitiesModule::class, UseCaseModule::class, RemoteRepositoryModule::class, ViewModelModule::class, FragmentsModule::class))
interface ActivitiesComponent {

    fun inject(splashActivity: SplashActivity)

    fun inject(mainActivity: MainActivity)

    fun inject(detailActivity: DetailActivity)

    fun inject(playersPerspectiveFragment: PlayerPerspectiveFragment)

    fun inject(developersFragment: DevelopersFragment)

    fun inject(platformsFragment: PlatformsFragment)

    fun inject(relatedGamesFragment: RelatedGamesFragment)

    fun inject(gameImagesFragment: GameImagesFragment)

    fun inject(listFragment: ListFragment)


}