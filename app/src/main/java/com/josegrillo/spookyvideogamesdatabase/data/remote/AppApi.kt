package com.josegrillo.kotlinmvp.data.remote

import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import com.josegrillo.kotlinmvp.utils.AppConstants
import com.josegrillo.spookyvideogamesdatabase.domain.model.api.response.*
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.internal.platform.Platform
import retrofit2.Response
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Url


interface AppApi {

    @GET("games/")
    fun gamesList(@Query("fields") fields: String, @Query("limit") limit: Int,
                  @Query("scroll") scroll: Int,
                  @Query("filter[name][any]") filterNameGames: String,
                  @Query("order") popularity: String): Observable<Response<List<Game>>>

    @GET
    fun gameScroll(@Url scroll: String): Observable<List<Game>>

    @GET("games/{id}")
    fun gameById(@Path("id") ids: String): Observable<List<Game>>

    @GET("collections/{id}")
    fun gameCollections(@Path("id") ids: String, @Query("fields") fields: String,
                        @Query("limit") limit: Int): Observable<List<GameCollection>>

    @GET("companies/{id}")
    fun companiesById(@Path("id") ids: String,
                      @Query("fields") fields: String): Observable<List<Developer>>

    @GET("player_perspectives/{id}")
    fun playersPerspective(@Path("id") ids: String,
                           @Query("fields") fields: String): Observable<List<PlayerPerspective>>

    @GET("platforms/{id}")
    fun platforms(@Path("id") ids: String,
                  @Query("fields") fields: String): Observable<List<GamePlatform>>


    companion object Factory {
        fun create(baseUrl: String): AppApi {

            val httpClient = OkHttpClient.Builder()

            httpClient.addInterceptor(LoggingInterceptor.Builder()
                    .loggable(false)
                    .setLevel(Level.BODY)
                    .setLevel(Level.HEADERS)
                    .log(Platform.INFO)
                    .request(AppConstants.REQUEST_TAG)
                    .response(AppConstants.RESPONSE_TAG)
                    .addHeader(AppConstants.HEADER_ACCEPT_TAG, AppConstants.HEADER_APPLICATION_JSON_TAG)
                    .addHeader(AppConstants.USER_KEY_TAG, AppConstants.USER_KEY_VALUE)
                    .build())

            val retrofit = retrofit2.Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .baseUrl(baseUrl)
                    .build()

            return retrofit.create(AppApi::class.java)
        }
    }

}