package com.josegrillo.spookyvideogamesdatabase.view.model

data class GamePlatform(val name: String,
                        val idServer: Long,
                        val logo: Image? = null)