package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.josegrillo.kotlinmvp.di.component.DaggerActivitiesComponent
import com.josegrillo.spookyvideogamesdatabase.R
import com.josegrillo.spookyvideogamesdatabase.di.module.FragmentsModule
import com.josegrillo.spookyvideogamesdatabase.view.base.BaseFragment
import com.josegrillo.spookyvideogamesdatabase.view.model.Image
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.adapter.ImageAdapter
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.interfaces.DisplayDialogImageOnTouch
import com.josegrillo.spookyvideogamesdatabase.view.utils.DialogUtils
import kotlinx.android.synthetic.main.fragment_game_images.view.*

private const val ARG_PARAM_IMAGES_LIST_IDS = "imagesList"

class GameImagesFragment : BaseFragment(), DisplayDialogImageOnTouch {

    private var imageList: ArrayList<Image>? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        arguments?.let {
            imageList = it.getParcelableArrayList(ARG_PARAM_IMAGES_LIST_IDS)
        }

        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val rootView = inflater.inflate(R.layout.fragment_game_images, container, false)

        imageList?.let {

            rootView.fragmentGameImagesRecyclerView.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
            rootView.fragmentGameImagesRecyclerView.adapter = ImageAdapter(it, this)

        }

        return rootView
    }


    override fun injectDependency() {

        val gameImagesComponent = DaggerActivitiesComponent.builder()
                .fragmentsModule(FragmentsModule())
                .build()

        gameImagesComponent.inject(this)

    }

    override fun displayDialogImage(image: Image) {

        DialogUtils.displayImageDialog(image, getParentActivity())

    }


    companion object {

        @JvmStatic
        fun newInstance(imagesList: ArrayList<Image>) =
                GameImagesFragment().apply {
                    arguments = Bundle().apply {
                        putParcelableArrayList(ARG_PARAM_IMAGES_LIST_IDS, imagesList)
                    }
                }
    }

}