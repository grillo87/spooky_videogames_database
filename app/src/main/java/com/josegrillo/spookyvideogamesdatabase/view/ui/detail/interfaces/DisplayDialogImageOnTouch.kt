package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.interfaces

import com.josegrillo.spookyvideogamesdatabase.view.model.Image

interface DisplayDialogImageOnTouch {

    fun displayDialogImage(image: Image)

}