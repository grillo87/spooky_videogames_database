package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.josegrillo.kotlinmvp.di.component.DaggerActivitiesComponent
import com.josegrillo.spookyvideogamesdatabase.R
import com.josegrillo.spookyvideogamesdatabase.databinding.FragmentRelatedGamesBinding
import com.josegrillo.spookyvideogamesdatabase.di.module.FragmentsModule
import com.josegrillo.spookyvideogamesdatabase.view.base.BaseFragment
import com.josegrillo.spookyvideogamesdatabase.view.model.Game
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.adapter.RelatedGameAdapter
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.interfaces.RecyclerViewObserver
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.interfaces.RelatedGamesTouch
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewModel.RelatedGamesViewModel
import com.josegrillo.spookyvideogamesdatabase.view.utils.NavigatorUtils
import kotlinx.android.synthetic.main.fragment_related_games.view.*
import javax.inject.Inject

private const val ARG_PARAM_GAME_COLLECTIONS_IDS = "gameCollectionsId"

class RelatedGamesFragment : BaseFragment(), RelatedGamesTouch, RecyclerViewObserver {

    private var gameCollectionsIds: String? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var fragmentRelatedGamesBinding: FragmentRelatedGamesBinding
    lateinit var relatedGamesViewModel: RelatedGamesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {

        arguments?.let {
            gameCollectionsIds = it.getString(ARG_PARAM_GAME_COLLECTIONS_IDS)
        }

        super.onCreate(savedInstanceState)


    }


    override fun injectDependency() {

        val relatedGamesComponent = DaggerActivitiesComponent.builder()
                .fragmentsModule(FragmentsModule())
                .build()

        relatedGamesComponent.inject(this)

    }

    override fun initializeDataBinding() {

        relatedGamesViewModel = ViewModelProviders.of(this, viewModelFactory)[RelatedGamesViewModel::class.java]

        gameCollectionsIds?.let {

            relatedGamesViewModel.loadRelatedGames(it)

        }

    }

    override fun initializeObservers() {

        initializeRecyclerViewObserver()

        initializeErrorListener()

    }

    override fun initializeRecyclerViewObserver() {

        relatedGamesViewModel.relatedGamesList.observe(this, Observer { relatedGames ->

            relatedGames?.let {

                fragmentRelatedGamesBinding.root.fragmentRelatedGamesRecyclerView.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
                fragmentRelatedGamesBinding.root.fragmentRelatedGamesRecyclerView.adapter = RelatedGameAdapter(it, this)


            }

        })


    }

    override fun initializeErrorListener() {

        relatedGamesViewModel.errorStatus.observe(this, Observer {


            it?.let {

                this.getParentActivity().displayErrorMessage(it)

            }


        })

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        fragmentRelatedGamesBinding =
                DataBindingUtil.inflate(
                        inflater, R.layout.fragment_related_games, container, false)
        fragmentRelatedGamesBinding.relatedGamesViewModel = relatedGamesViewModel
        fragmentRelatedGamesBinding.setLifecycleOwner(this)

        return fragmentRelatedGamesBinding.root
    }


    override fun onRelatedGameTouch(gameSelected: Game) {

        NavigatorUtils.navigateToDetailActivity(getParentActivity(), gameSelected)

    }


    companion object {

        @JvmStatic
        fun newInstance(gameCollectionsIds: String) =
                RelatedGamesFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM_GAME_COLLECTIONS_IDS, gameCollectionsIds)
                    }
                }
    }

}