package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewModel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.josegrillo.spookyvideogamesdatabase.domain.model.mapper.PlayerPerspectiveMapper
import com.josegrillo.spookyvideogamesdatabase.domain.usecase.GetPlayersPerspectives
import com.josegrillo.spookyvideogamesdatabase.view.model.PlayersPerspective
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PlayerPerspectivesViewModel @Inject constructor(val getPlayersPerspective: GetPlayersPerspectives) : ViewModel() {

    val playersPerspectivesList = MutableLiveData<ArrayList<PlayersPerspective>>()
    val playersPerspectiveLoadingStatus = MutableLiveData<Boolean>()
    val errorStatus = MutableLiveData<Throwable>()


    fun loadPlayersPerspective(idsPerspectives: String?) {

        errorStatus.value = null

        idsPerspectives?.let {

            getPlayersPerspective.getPlayersPerspective(it)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { playersPerspectives ->

                                playersPerspectives?.let {

                                    playersPerspectiveLoadingStatus.value = false


                                    if (it.isNotEmpty()) {

                                        playersPerspectivesList.value = PlayerPerspectiveMapper.playerPerspectiveApiListToVieModelList(it)

                                    }


                                }

                            },
                            { error ->
                                playersPerspectiveLoadingStatus.value = false
                                errorStatus.value = error
                            })


        }


    }


}