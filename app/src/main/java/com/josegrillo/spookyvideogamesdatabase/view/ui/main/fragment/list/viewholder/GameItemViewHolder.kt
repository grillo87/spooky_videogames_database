package com.josegrillo.spookyvideogamesdatabase.view.ui.main.fragment.list.viewholder

import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import com.josegrillo.spookyvideogamesdatabase.databinding.GameItemViewholderBinding
import com.josegrillo.spookyvideogamesdatabase.view.model.Game
import com.josegrillo.spookyvideogamesdatabase.view.ui.main.fragment.list.interfaces.GameSelectedTouch

class GameItemViewHolder(val gameItemViewholderBinding: GameItemViewholderBinding,
                         val gameSelectedTouch: GameSelectedTouch,
                         val customFont: Typeface?) : RecyclerView.ViewHolder(gameItemViewholderBinding.root) {

    fun bind(game: Game?) {

        game?.let {

            this.customFont?.let {

                gameItemViewholderBinding.gameItemViewholderTitleTextview.typeface = customFont

            }

            gameItemViewholderBinding.game = game
            gameItemViewholderBinding.gameSelectedTouch = gameSelectedTouch
            gameItemViewholderBinding.executePendingBindings()

        }

    }


}