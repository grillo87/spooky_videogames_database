package com.josegrillo.spookyvideogamesdatabase.view.utils

import android.content.Intent
import com.josegrillo.kotlinmvp.view.base.BaseActivity
import com.josegrillo.spookyvideogamesdatabase.view.model.Game
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.activity.DetailActivity
import com.josegrillo.spookyvideogamesdatabase.view.ui.main.activity.MainActivity

class NavigatorUtils {

    companion object {

        val ID_GAME_PARAM = "idGame"


        fun navigateToMainActivity(activity: BaseActivity) {

            val mainListIntent = Intent().setClass(
                    activity, MainActivity::class.java)
            activity.startActivity(mainListIntent)
            activity.finish()

        }


        fun navigateToDetailActivity(activity: BaseActivity, game: Game) {

            val detailIntent = Intent().setClass(
                    activity, DetailActivity::class.java)
            detailIntent.putExtra(ID_GAME_PARAM, game.idServer.toString())
            activity.startActivity(detailIntent)
            activity.finish()

        }

    }

}