package com.josegrillo.spookyvideogamesdatabase.view.ui.main.fragment.list.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.josegrillo.kotlinmvp.utils.AppConstants
import com.josegrillo.spookyvideogamesdatabase.domain.model.mapper.GameMapper
import com.josegrillo.spookyvideogamesdatabase.domain.usecase.GetGamesList
import com.josegrillo.spookyvideogamesdatabase.domain.usecase.GetGamesScroll
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class ListViewModel @Inject constructor(val getGamesList: GetGamesList,
                                        val getGamesScroll: GetGamesScroll) : ViewModel() {

    val gameList = MutableLiveData<ArrayList<com.josegrillo.spookyvideogamesdatabase.view.model.Game>>()
    val gameListAditional = MutableLiveData<ArrayList<com.josegrillo.spookyvideogamesdatabase.view.model.Game>>()
    val loadingStatus = MutableLiveData<Boolean>()
    val errorStatus = MutableLiveData<Throwable>()

    var nextScroll: String? = null

    fun loadInitialGameList() {

        loadingStatus.value = true
        errorStatus.value = null

        getGamesList.getGamesList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { response ->

                            response?.let {

                                nextScroll = response.headers()["x-next-page"]

                                response.body()?.let {

                                    loadingStatus.value = false
                                    gameList.value = GameMapper.gamesApiResultToGamesViewModel(it)


                                }


                            }

                        },

                        { error ->

                            loadingStatus.value = false
                            errorStatus.value = error


                        })

    }


    fun loadScrollGameList() {

        errorStatus.value = null

        nextScroll?.let {

            getGamesScroll.getGameScroll(AppConstants.BASE_URL + it)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ gameListAppend ->

                        gameListAppend?.let {

                            gameListAditional.value = GameMapper.gamesApiResultToGamesViewModel(it)

                        }


                    },
                            { error ->
                                errorStatus.value = error
                            })


        }


    }


}