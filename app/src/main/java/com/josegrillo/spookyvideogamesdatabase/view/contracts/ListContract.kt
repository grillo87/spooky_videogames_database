package com.josegrillo.spookyvideogamesdatabase.view.contracts

interface ListContract {

    fun initialGameObserver()

    fun initializeScrollListener()

    fun aditionalGameObserver()

}