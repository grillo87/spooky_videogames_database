package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewholder

import android.support.v7.widget.RecyclerView
import com.josegrillo.spookyvideogamesdatabase.databinding.ImageItemViewHolderBinding
import com.josegrillo.spookyvideogamesdatabase.view.model.Image
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.interfaces.DisplayDialogImageOnTouch

class ImageViewHolder(val imageItemViewHolderBinding: ImageItemViewHolderBinding,
                      val displayDialogImageOnTouch: DisplayDialogImageOnTouch) : RecyclerView.ViewHolder(imageItemViewHolderBinding.root) {


    fun bind(image: Image) {

        imageItemViewHolderBinding.image = image
        imageItemViewHolderBinding.displayDialogImage = displayDialogImageOnTouch
        imageItemViewHolderBinding.executePendingBindings()

    }

}