package com.josegrillo.spookyvideogamesdatabase.view.model

data class Game(val idServer: Long,
                val name: String,
                val summary: String? = null,
                val collection: Long? = null,
                val rating: Double? = null,
                var developers: Developer? = null,
                val esrbRating: GameRating? = null,
                val timeToBeat: String? = null,
                val releaseDate: String? = null,
                val screenshots: ArrayList<String>? = null,
                val cover: Image? = null,
                val artworks: ArrayList<String>? = null)