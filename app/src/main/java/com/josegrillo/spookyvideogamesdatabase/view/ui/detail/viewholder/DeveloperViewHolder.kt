package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewholder

import android.support.v7.widget.RecyclerView
import com.josegrillo.spookyvideogamesdatabase.databinding.DeveloperItemViewHolderBinding
import com.josegrillo.spookyvideogamesdatabase.databinding.PlayersPerspectiveItemViewHolderBinding
import com.josegrillo.spookyvideogamesdatabase.view.model.Developer
import com.josegrillo.spookyvideogamesdatabase.view.model.PlayersPerspective

class DeveloperViewHolder(val developerItemViewHolderBinding: DeveloperItemViewHolderBinding) : RecyclerView.ViewHolder(developerItemViewHolderBinding.root) {


    fun bind(developer: Developer) {

        developerItemViewHolderBinding.developer = developer
        developerItemViewHolderBinding.executePendingBindings()

    }

}