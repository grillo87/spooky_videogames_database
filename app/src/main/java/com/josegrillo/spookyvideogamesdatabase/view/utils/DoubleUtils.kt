package com.josegrillo.spookyvideogamesdatabase.view.utils

class DoubleUtils {

    companion object {

        fun gameRatingFormat(gameRating: Double): String {

            var result = StringUtils.DEFAULT_NA

            try {

                result = "${gameRating.format(2)} of 100"

            } catch (e: Exception) {


            }

            return result

        }

        fun Double.format(digits: Int) = java.lang.String.format("%.${digits}f", this)

    }

}