package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.josegrillo.spookyvideogamesdatabase.R
import com.josegrillo.spookyvideogamesdatabase.databinding.DeveloperItemViewHolderBinding
import com.josegrillo.spookyvideogamesdatabase.databinding.PlayersPerspectiveItemViewHolderBinding
import com.josegrillo.spookyvideogamesdatabase.view.model.Developer
import com.josegrillo.spookyvideogamesdatabase.view.model.PlayersPerspective
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewholder.DeveloperViewHolder
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewholder.PlayerPerspectiveViewHolder

class DeveloperAdapter(val developers: ArrayList<Developer>) : RecyclerView.Adapter<DeveloperViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeveloperViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val developerItemViewHolderBinding: DeveloperItemViewHolderBinding = DataBindingUtil.inflate(
                layoutInflater, R.layout.developer_item_view_holder, parent, false)
        return DeveloperViewHolder(developerItemViewHolderBinding)

    }

    override fun getItemCount(): Int = developers.size

    override fun onBindViewHolder(holder: DeveloperViewHolder, position: Int) {

        holder.bind(developers[position])

    }
}