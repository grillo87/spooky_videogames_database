package com.josegrillo.spookyvideogamesdatabase.view.utils

import java.text.SimpleDateFormat
import java.util.*

class StringUtils {

    companion object {

        val DEFAULT_URL_BASE = "http:"
        val COVER_BIG_VALUE = "t_cover_big"
        val LOGO_MED_VALUE = "t_logo_med"

        val DEFAULT_NA = "NA"
        val DEFAULT_RELEASE = "Released on "
        val DEFAULT_NOT_RELEASED = "Not yet released"
        val DATE_FORMAT = "MMM dd, yyyy"

        fun formatImageUrl(urlApi: String?, newFormatUrl: String): String {

            var result = DEFAULT_URL_BASE

            urlApi?.let {

                val urlFormatBigCover = it.replace("t_thumb", newFormatUrl)
                result += urlFormatBigCover

            }

            return result

        }


        fun formatDateGame(unformatDate: Long?, desiredFormat: String): String {

            var result = DEFAULT_NOT_RELEASED

            try {

                val sdf = SimpleDateFormat(desiredFormat, Locale.US)
                val formatDate = Date(unformatDate!!)
                result = DEFAULT_RELEASE + sdf.format(formatDate)

            } catch (e: Exception) {


            }

            return result

        }


        fun timeToBeatFormat(timeToBeat: Long?): String {

            var result = DEFAULT_NA

            try {

                val hours: Long? = timeToBeat?.div(60L)?.div(60L)
                val minutes: Long? = (timeToBeat?.minus(hours!!.times(3600L)))?.div(60L)
                result = "$hours hrs $minutes min"

            } catch (e: Exception) {


            }

            return result

        }


    }


}