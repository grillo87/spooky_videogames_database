package com.josegrillo.spookyvideogamesdatabase.view.ui.main.fragment.list.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.josegrillo.kotlinmvp.di.component.DaggerActivitiesComponent
import com.josegrillo.spookyvideogamesdatabase.R
import com.josegrillo.spookyvideogamesdatabase.databinding.FragmentListBinding
import com.josegrillo.spookyvideogamesdatabase.di.module.FragmentsModule
import com.josegrillo.spookyvideogamesdatabase.view.base.BaseFragment
import com.josegrillo.spookyvideogamesdatabase.view.contracts.ListContract
import com.josegrillo.spookyvideogamesdatabase.view.model.Game
import com.josegrillo.spookyvideogamesdatabase.view.ui.main.fragment.list.adapter.GameAdapter
import com.josegrillo.spookyvideogamesdatabase.view.ui.main.fragment.list.interfaces.GameSelectedTouch
import com.josegrillo.spookyvideogamesdatabase.view.ui.main.fragment.list.viewmodel.ListViewModel
import com.josegrillo.spookyvideogamesdatabase.view.utils.NavigatorUtils
import kotlinx.android.synthetic.main.fragment_list.view.*
import javax.inject.Inject

class ListFragment : BaseFragment(), ListContract, GameSelectedTouch {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var listViewModel: ListViewModel
    lateinit var fragmentListBinding: FragmentListBinding

    private lateinit var gameAdapter: GameAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private val lastVisibleItemPosition: Int
        get() = linearLayoutManager.findLastVisibleItemPosition()
    private lateinit var scrollListener: RecyclerView.OnScrollListener


    override fun injectDependency() {

        val listComponent = DaggerActivitiesComponent.builder()
                .fragmentsModule(FragmentsModule())
                .build()

        listComponent.inject(this)

    }

    override fun initializeDataBinding() {

        listViewModel = ViewModelProviders.of(this, viewModelFactory)[ListViewModel::class.java]
        listViewModel.loadInitialGameList()

    }

    override fun initializeScrollListener() {

        scrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val totalItemCount = recyclerView!!.layoutManager.itemCount
                if (totalItemCount == lastVisibleItemPosition + 1) {

                    listViewModel.loadScrollGameList()
                    recyclerView.fragmentListGamesRecyclerView.removeOnScrollListener(scrollListener)
                }
            }
        }


    }


    override fun initialGameObserver() {

        listViewModel.gameList.observe(this, Observer { gamesList ->

            gamesList?.let {

                fragmentListBinding.root.fragmentListGamesRecyclerView.layoutManager = linearLayoutManager
                gameAdapter = GameAdapter(it, this, getParentActivity().customFont)

                fragmentListBinding.root.fragmentListGamesRecyclerView.adapter = gameAdapter
                fragmentListBinding.root.fragmentListGamesRecyclerView.addOnScrollListener(scrollListener)


            }

        })

    }


    override fun aditionalGameObserver() {

        listViewModel.gameListAditional.observe(this, Observer { gamesListAditional ->

            gamesListAditional?.let {

                gameAdapter.appendElements(it)
                fragmentListBinding.root.fragmentListGamesRecyclerView.addOnScrollListener(scrollListener)


            }

        })

    }

    override fun initializeObservers() {


        initialGameObserver()

        aditionalGameObserver()

        initializeScrollListener()

        initializeErrorListener()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        linearLayoutManager = LinearLayoutManager(this.context)


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        fragmentListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_list, container, false)
        listViewModel = ViewModelProviders.of(this).get(ListViewModel::class.java)
        fragmentListBinding.listViewModel = listViewModel
        fragmentListBinding.setLifecycleOwner(this)

        return fragmentListBinding.root

    }

    override fun onGameSelected(game: Game) {

        NavigatorUtils.navigateToDetailActivity(getParentActivity(), game)

    }

    override fun initializeErrorListener() {

        listViewModel.errorStatus.observe(this, Observer {


            it?.let {

                this.getParentActivity().displayErrorMessage(it)

            }


        })

    }

}
