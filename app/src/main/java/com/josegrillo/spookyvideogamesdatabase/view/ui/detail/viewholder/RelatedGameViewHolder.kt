package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewholder

import android.support.v7.widget.RecyclerView
import com.josegrillo.spookyvideogamesdatabase.databinding.RelatedGameItemViewHolderBinding
import com.josegrillo.spookyvideogamesdatabase.view.model.Game
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.interfaces.RelatedGamesTouch

class RelatedGameViewHolder(val relatedGameItemViewHolderBinding: RelatedGameItemViewHolderBinding,
                            val relatedGamesTouch: RelatedGamesTouch) : RecyclerView.ViewHolder(relatedGameItemViewHolderBinding.root) {


    fun bind(relatedGame: Game) {

        relatedGameItemViewHolderBinding.relatedGame = relatedGame
        relatedGameItemViewHolderBinding.selectedRelatedGame = relatedGamesTouch
        relatedGameItemViewHolderBinding.executePendingBindings()

    }

}