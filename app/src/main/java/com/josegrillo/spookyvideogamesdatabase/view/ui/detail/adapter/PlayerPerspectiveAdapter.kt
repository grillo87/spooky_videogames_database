package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.josegrillo.spookyvideogamesdatabase.R
import com.josegrillo.spookyvideogamesdatabase.databinding.PlayersPerspectiveItemViewHolderBinding
import com.josegrillo.spookyvideogamesdatabase.view.model.PlayersPerspective
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewholder.PlayerPerspectiveViewHolder

class PlayerPerspectiveAdapter(val playerPerspectives: ArrayList<PlayersPerspective>) : RecyclerView.Adapter<PlayerPerspectiveViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerPerspectiveViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val playersPerspectiveItemViewHolderBinding: PlayersPerspectiveItemViewHolderBinding = DataBindingUtil.inflate(
                layoutInflater, R.layout.players_perspective_item_view_holder, parent, false)
        return PlayerPerspectiveViewHolder(playersPerspectiveItemViewHolderBinding)

    }

    override fun getItemCount(): Int = playerPerspectives.size

    override fun onBindViewHolder(holder: PlayerPerspectiveViewHolder, position: Int) {

        holder.bind(playerPerspectives[position])

    }
}