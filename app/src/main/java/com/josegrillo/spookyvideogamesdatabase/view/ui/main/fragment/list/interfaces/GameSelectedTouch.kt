package com.josegrillo.spookyvideogamesdatabase.view.ui.main.fragment.list.interfaces

import com.josegrillo.spookyvideogamesdatabase.view.model.Game

interface GameSelectedTouch {

    fun onGameSelected(game: Game)

}