package com.josegrillo.kotlinmvp.view.base

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.graphics.Typeface
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.CoordinatorLayout
import android.support.v7.app.AppCompatActivity
import android.text.Layout
import android.view.View
import com.josegrillo.kotlinmvp.view.utils.ScreenUtils
import com.josegrillo.spookyvideogamesdatabase.R
import com.josegrillo.spookyvideogamesdatabase.view.utils.ErrorUtils
import com.josegrillo.spookyvideogamesdatabase.view.utils.ToastUtils
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.container_loading_layout.*

open class BaseActivity : AppCompatActivity() {

    var customFont: Typeface? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        injectDependency()
        defineViewModel()
        initializeDataBinding()
        defineCustomFont()
        setCustomFonts()
        setStatusBar()
        setAppOrientation()
        initializeOnClickListeners()
        initializeDrawerMenu()
        initializeSupportActionBar()
        initializeObservers()

    }

    private fun defineCustomFont() {

        this.customFont = Typeface.createFromAsset(applicationContext.assets, "fonts/vademecum.ttf")
    }

    protected inline fun <reified T : ViewModel> getViewModel(): T =
            ViewModelProviders.of(this)[T::class.java]


    private fun setStatusBar() {

        ScreenUtils.tintStatusBar(this)

    }


    private fun setAppOrientation() {

        ScreenUtils.setPortaitOrientation(this)

    }

    open fun displayErrorMessage(error: Throwable?) {

        var messageToDisplay = this.resources.getString(R.string.unexpectedErrorMessage)
        error?.let {

            messageToDisplay = ErrorUtils.errorHandler(it, applicationContext)

        }

        ToastUtils.showToastMessage(applicationContext, messageToDisplay)

    }


    open fun setCustomFonts() {}

    open fun injectDependency() {}

    open fun defineViewModel() {}

    open fun initializeOnClickListeners() {}

    open fun initializeSupportActionBar() {}

    open fun initializeDrawerMenu() {}

    open fun initializeObservers() {}

    open fun initializeDataBinding() {}

    open fun initializeErrorListener() {}


}