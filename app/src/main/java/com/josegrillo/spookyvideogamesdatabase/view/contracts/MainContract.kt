package com.josegrillo.spookyvideogamesdatabase.view.contracts

interface MainContract {

    fun navigateToInitialOption()

    fun displayHomeView(): Boolean

    fun navigateToGitlabRepository(): Boolean

    fun navigateToGooglePlay(): Boolean

    fun loadBanner()


}