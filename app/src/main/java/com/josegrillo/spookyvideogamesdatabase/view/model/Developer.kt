package com.josegrillo.spookyvideogamesdatabase.view.model

class Developer(var name: String,
                val logo: Image? = null)