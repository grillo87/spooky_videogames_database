package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewholder

import android.support.v7.widget.RecyclerView
import com.josegrillo.spookyvideogamesdatabase.databinding.PlatformItemViewHolderBinding
import com.josegrillo.spookyvideogamesdatabase.view.model.GamePlatform

class PlatformViewHolder(val platformItemViewHolderBinding: PlatformItemViewHolderBinding) : RecyclerView.ViewHolder(platformItemViewHolderBinding.root) {


    fun bind(gamePlatform: GamePlatform) {

        platformItemViewHolderBinding.gamePlatform = gamePlatform
        platformItemViewHolderBinding.executePendingBindings()

    }

}