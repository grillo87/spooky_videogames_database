package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.fragment


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.josegrillo.kotlinmvp.di.component.DaggerActivitiesComponent
import com.josegrillo.spookyvideogamesdatabase.R
import com.josegrillo.spookyvideogamesdatabase.databinding.FragmentPlayerPerspectiveBinding
import com.josegrillo.spookyvideogamesdatabase.di.module.FragmentsModule
import com.josegrillo.spookyvideogamesdatabase.view.base.BaseFragment
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.adapter.PlayerPerspectiveAdapter
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.interfaces.RecyclerViewObserver
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewModel.PlayerPerspectivesViewModel
import kotlinx.android.synthetic.main.fragment_player_perspective.view.*
import javax.inject.Inject

private const val ARG_PARAM_PERSPECTIVES_IDS = "perspectivesIds"

class PlayerPerspectiveFragment : BaseFragment(), RecyclerViewObserver {

    private var perspectivesIds: String? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var fragmentPlayerPerspectiveDataBinding: FragmentPlayerPerspectiveBinding
    lateinit var playerPerspectivesViewModel: PlayerPerspectivesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {

        arguments?.let {
            perspectivesIds = it.getString(ARG_PARAM_PERSPECTIVES_IDS)
        }

        super.onCreate(savedInstanceState)


    }


    override fun injectDependency() {

        val playersPerspectiveComponent = DaggerActivitiesComponent.builder()
                .fragmentsModule(FragmentsModule())
                .build()

        playersPerspectiveComponent.inject(this)

    }

    override fun initializeDataBinding() {

        playerPerspectivesViewModel = ViewModelProviders.of(this, viewModelFactory)[PlayerPerspectivesViewModel::class.java]

        perspectivesIds?.let {

            playerPerspectivesViewModel.loadPlayersPerspective(it)

        }

    }

    override fun initializeObservers() {

        initializeRecyclerViewObserver()

        initializeErrorListener()

    }


    override fun initializeRecyclerViewObserver() {

        playerPerspectivesViewModel.playersPerspectivesList.observe(this, Observer {playerPerspectives ->

            playerPerspectives?.let {

                fragmentPlayerPerspectiveDataBinding.root.fragmentPlayersPerspectiveRecyclerView.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL ,false)
                fragmentPlayerPerspectiveDataBinding.root.fragmentPlayersPerspectiveRecyclerView.adapter = PlayerPerspectiveAdapter(it)


            }

        })


    }


    override fun initializeErrorListener() {

        playerPerspectivesViewModel.errorStatus.observe(this, Observer {


            it?.let {

                this.getParentActivity().displayErrorMessage(it)

            }


        })

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        fragmentPlayerPerspectiveDataBinding =
                DataBindingUtil.inflate(
                        inflater, R.layout.fragment_player_perspective, container, false)
        fragmentPlayerPerspectiveDataBinding.playersPerspectiveViewModel = playerPerspectivesViewModel
        fragmentPlayerPerspectiveDataBinding.setLifecycleOwner(this)

        return fragmentPlayerPerspectiveDataBinding.root
    }


    companion object {

        @JvmStatic
        fun newInstance(perspectivesIds: String) =
                PlayerPerspectiveFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM_PERSPECTIVES_IDS, perspectivesIds)
                    }
                }
    }
}
