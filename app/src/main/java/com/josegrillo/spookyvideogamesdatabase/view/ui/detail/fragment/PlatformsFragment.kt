package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.josegrillo.kotlinmvp.di.component.DaggerActivitiesComponent
import com.josegrillo.spookyvideogamesdatabase.R
import com.josegrillo.spookyvideogamesdatabase.databinding.FragmentPlatformsBinding
import com.josegrillo.spookyvideogamesdatabase.di.module.FragmentsModule
import com.josegrillo.spookyvideogamesdatabase.view.base.BaseFragment
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.adapter.PlatformAdapter
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.interfaces.RecyclerViewObserver
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewModel.PlatformViewModel
import kotlinx.android.synthetic.main.fragment_platforms.view.*
import javax.inject.Inject

private const val ARG_PARAM_PLATFORMS_IDS = "platformsIds"

class PlatformsFragment : BaseFragment(), RecyclerViewObserver {

    private var platformsIds: String? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var fragmentPlatformsBinding: FragmentPlatformsBinding
    lateinit var platformViewModel: PlatformViewModel

    override fun onCreate(savedInstanceState: Bundle?) {

        arguments?.let {
            platformsIds = it.getString(ARG_PARAM_PLATFORMS_IDS)
        }

        super.onCreate(savedInstanceState)


    }


    override fun injectDependency() {

        val platformsComponent = DaggerActivitiesComponent.builder()
                .fragmentsModule(FragmentsModule())
                .build()

        platformsComponent.inject(this)

    }

    override fun initializeDataBinding() {

        platformViewModel = ViewModelProviders.of(this, viewModelFactory)[PlatformViewModel::class.java]

        platformsIds?.let {

            platformViewModel.loadGamePlatforms(it)

        }

    }

    override fun initializeObservers() {

        initializeRecyclerViewObserver()

        initializeErrorListener()

    }

    override fun initializeRecyclerViewObserver() {

        platformViewModel.platformsList.observe(this, Observer { gamePlatforms ->

            gamePlatforms?.let {

                fragmentPlatformsBinding.root.fragmentPlatformsRecyclerView.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
                fragmentPlatformsBinding.root.fragmentPlatformsRecyclerView.adapter = PlatformAdapter(it)


            }

        })


    }


    override fun initializeErrorListener() {

        platformViewModel.errorStatus.observe(this, Observer {


            it?.let {

                this.getParentActivity().displayErrorMessage(it)

            }


        })

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        fragmentPlatformsBinding =
                DataBindingUtil.inflate(
                        inflater, R.layout.fragment_platforms, container, false)
        fragmentPlatformsBinding.platformsViewModel = platformViewModel
        fragmentPlatformsBinding.setLifecycleOwner(this)

        return fragmentPlatformsBinding.root
    }


    companion object {

        @JvmStatic
        fun newInstance(platformsIds: String) =
                PlatformsFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM_PLATFORMS_IDS, platformsIds)
                    }
                }
    }

}