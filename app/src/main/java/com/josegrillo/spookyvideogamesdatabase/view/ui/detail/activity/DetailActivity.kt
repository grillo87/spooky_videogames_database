package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.josegrillo.kotlinmvp.di.component.DaggerActivitiesComponent
import com.josegrillo.kotlinmvp.di.module.ActivitiesModule
import com.josegrillo.kotlinmvp.view.base.BaseActivity
import com.josegrillo.spookyvideogamesdatabase.R
import com.josegrillo.spookyvideogamesdatabase.databinding.ActivityDetailBinding
import com.josegrillo.spookyvideogamesdatabase.view.contracts.DetailContract
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.fragment.*
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewModel.DetailViewModel
import com.josegrillo.spookyvideogamesdatabase.view.utils.FragmentUtils
import com.josegrillo.spookyvideogamesdatabase.view.utils.NavigatorUtils
import kotlinx.android.synthetic.main.activity_detail.*
import javax.inject.Inject

class DetailActivity : BaseActivity(), DetailContract {

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {

            android.R.id.home -> NavigatorUtils.navigateToMainActivity(this)

        }

        return false
    }


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var activityDetailBinding: ActivityDetailBinding
    lateinit var detailViewModel: DetailViewModel
    var gameId: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {

        gameId = intent?.extras?.getString(NavigatorUtils.ID_GAME_PARAM)
        super.onCreate(savedInstanceState)

        loadBanner()


    }

    override fun injectDependency() {
        val detailComponent = DaggerActivitiesComponent.builder()
                .activitiesModule(ActivitiesModule())
                .build()

        detailComponent.inject(this)
    }


    override fun defineViewModel() {

        detailViewModel = ViewModelProviders.of(this, viewModelFactory)[DetailViewModel::class.java]

        gameId?.let {

            detailViewModel.loadGameDetail(it)

        }


    }


    override fun initializeDataBinding() {

        activityDetailBinding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        activityDetailBinding.gameDetailViewModel = detailViewModel
        activityDetailBinding.setLifecycleOwner(this)

    }


    override fun initializeSupportActionBar() {

        setSupportActionBar(activityToolBar)
        supportActionBar?.title = resources.getString(R.string.gameDetailActivityTitle)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

    }

    override fun setCustomFonts() {

        containerDetailLayoutGameTitleTextview.typeface = this.customFont
        activityDetailSummaryTitleTextview.typeface = this.customFont
        activityDetailPlayerPerspectivesTitleTextview.typeface = this.customFont
        activityDetailDevelopersTitleTextview.typeface = this.customFont
        activityDetailPlatformTitleTextview.typeface = this.customFont
        activityDetailScreenshotsTitleTextview.typeface = this.customFont
        activityDetailArtWorksTitleTextview.typeface = this.customFont
        activityDetailRelatedGamesTitleTextview.typeface = this.customFont

    }

    override fun initializeObservers() {

        listenGamePerspectiveObserver()
        listenDevelopersObserver()
        listenGamePlatformsObserver()
        listenCollectionObserver()
        listenScreenshotsObserver()
        listenArtworksObserver()
        initializeErrorListener()

    }


    override fun onBackPressed() {
        NavigatorUtils.navigateToMainActivity(this)
    }

    override fun listenGamePerspectiveObserver() {

        detailViewModel.playersPerspectivesList.observe(this, Observer { playersPerspective ->

            playersPerspective?.let {

                activityDetailPlayerPerspectivesTitleTextview.visibility = View.VISIBLE
                activityDetailPerspectivesFrame.visibility = View.VISIBLE

                FragmentUtils.replaceFragment(activityDetailPerspectivesFrame.id, this, PlayerPerspectiveFragment.newInstance(it))

            }

        })

    }

    override fun listenDevelopersObserver() {

        detailViewModel.developersList.observe(this, Observer { developers ->

            developers?.let {

                activityDetailDevelopersTitleTextview.visibility = View.VISIBLE
                activityDetailDevelopersFrame.visibility = View.VISIBLE

                FragmentUtils.replaceFragment(activityDetailDevelopersFrame.id, this, DevelopersFragment.newInstance(it))

            }

        })

    }

    override fun listenGamePlatformsObserver() {

        detailViewModel.platformsList.observe(this, Observer { platforms ->

            platforms?.let {

                activityDetailPlatformTitleTextview.visibility = View.VISIBLE
                activityDetailPlatformFrame.visibility = View.VISIBLE

                FragmentUtils.replaceFragment(activityDetailPlatformFrame.id, this, PlatformsFragment.newInstance(it))

            }


        })

    }

    override fun listenCollectionObserver() {

        detailViewModel.collectionId.observe(this, Observer { collectionId ->

            collectionId?.let {

                activityDetailRelatedGamesTitleTextview.visibility = View.VISIBLE
                activityDetailRelatedGamesFrame.visibility = View.VISIBLE

                FragmentUtils.replaceFragment(activityDetailRelatedGamesFrame.id, this, RelatedGamesFragment.newInstance(it))

            }


        })

    }

    override fun listenScreenshotsObserver() {

        detailViewModel.screenshotsList.observe(this, Observer { screenshotsList ->

            screenshotsList?.let {

                activityDetailScreenshotsTitleTextview.visibility = View.VISIBLE
                activityDetailScreenshotsFrame.visibility = View.VISIBLE

                FragmentUtils.replaceFragment(activityDetailScreenshotsFrame.id, this, GameImagesFragment.newInstance(it))

            }


        })

    }

    override fun listenArtworksObserver() {

        detailViewModel.artWorksList.observe(this, Observer { artworksList ->

            artworksList?.let {

                activityDetailArtWorksTitleTextview.visibility = View.VISIBLE
                activityDetailArtWorksFrame.visibility = View.VISIBLE

                FragmentUtils.replaceFragment(activityDetailArtWorksFrame.id, this, GameImagesFragment.newInstance(it))

            }


        })

    }

    override fun initializeErrorListener() {

        detailViewModel.errorStatus.observe(this, Observer {

            it?.let {

                this.displayErrorMessage(it)

            }

        })

    }

    override fun loadBanner() {

        MobileAds.initialize(this, resources.getString(R.string.activityDetailBannerUnitid))
        val adRequest = AdRequest.Builder().build()
        activityDetailBannerAdview.loadAd(adRequest)

    }


}
