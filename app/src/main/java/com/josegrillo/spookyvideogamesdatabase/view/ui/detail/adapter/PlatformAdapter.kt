package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.josegrillo.spookyvideogamesdatabase.R
import com.josegrillo.spookyvideogamesdatabase.databinding.PlatformItemViewHolderBinding
import com.josegrillo.spookyvideogamesdatabase.view.model.GamePlatform
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewholder.PlatformViewHolder

class PlatformAdapter(val gamePlatforms: ArrayList<GamePlatform>) : RecyclerView.Adapter<PlatformViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlatformViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val platformItemViewHolderBinding: PlatformItemViewHolderBinding = DataBindingUtil.inflate(
                layoutInflater, R.layout.platform_item_view_holder, parent, false)
        return PlatformViewHolder(platformItemViewHolderBinding)

    }

    override fun getItemCount(): Int = gamePlatforms.size

    override fun onBindViewHolder(holder: PlatformViewHolder, position: Int) {

        holder.bind(gamePlatforms[position])

    }
}