package com.josegrillo.spookyvideogamesdatabase.view.model

import android.os.Parcel
import android.os.Parcelable

data class Image(val url: String? = null) : Parcelable {

    constructor(parcel: Parcel) : this(url = parcel.readString())

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(url)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Image> {
        override fun createFromParcel(parcel: Parcel): Image = Image(parcel)


        override fun newArray(size: Int): Array<Image?> = arrayOfNulls(size)

    }
}