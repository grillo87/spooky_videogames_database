package com.josegrillo.spookyvideogamesdatabase.view.utils

import android.databinding.BindingAdapter
import android.os.Build
import android.support.constraint.ConstraintLayout
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.josegrillo.kotlinmvp.utils.GlideApp
import com.josegrillo.spookyvideogamesdatabase.R
import com.josegrillo.spookyvideogamesdatabase.view.model.Game

class ViewModelUtils {

    companion object {

        @BindingAdapter("bind:imageUrl")
        @JvmStatic
        fun ImageView.setImageUrl(urlImage: String?) {

            if (urlImage != null) {

                GlideApp.with(context).load(urlImage)
                        .centerCrop()
                        .error(R.drawable.ic_spooky)
                        .placeholder(R.drawable.ic_spooky)
                        .diskCacheStrategy(DiskCacheStrategy.DATA)
                        .into(this)

            }

        }

        @BindingAdapter("bind:imageFitCenterUrl")
        @JvmStatic
        fun ImageView.setImageFitCenterUrl(urlImage: String?) {

            if (urlImage != null) {

                GlideApp.with(context).load(urlImage)
                        .fitCenter()
                        .error(R.drawable.ic_spooky)
                        .placeholder(R.drawable.ic_spooky)
                        .diskCacheStrategy(DiskCacheStrategy.DATA)
                        .into(this)

            }

        }

        @BindingAdapter("bind:gameRating")
        @JvmStatic
        fun TextView.setGameRating(gameRating: Double?) {

            var ratingText = context.resources.getString(R.string.na_value_tag)

            if (gameRating != null) {

                ratingText = DoubleUtils.gameRatingFormat(gameRating)

            }

            this.text = ratingText

        }


        @BindingAdapter("bind:loadingShow")
        @JvmStatic
        fun ConstraintLayout.setLoadingShow(show: Boolean?) {

            show?.let {

                if (it) {

                    this.visibility = View.VISIBLE

                } else {

                    this.visibility = View.GONE

                }

            }

        }


    }


}