package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.josegrillo.spookyvideogamesdatabase.R
import com.josegrillo.spookyvideogamesdatabase.databinding.RelatedGameItemViewHolderBinding
import com.josegrillo.spookyvideogamesdatabase.view.model.Game
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.interfaces.RelatedGamesTouch
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewholder.RelatedGameViewHolder

class RelatedGameAdapter(val relatedGames: ArrayList<Game>,
                         val relatedGamesTouch: RelatedGamesTouch) : RecyclerView.Adapter<RelatedGameViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RelatedGameViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val relatedGameItemViewHolderBinding: RelatedGameItemViewHolderBinding = DataBindingUtil.inflate(
                layoutInflater, R.layout.related_game_item_view_holder, parent, false)
        return RelatedGameViewHolder(relatedGameItemViewHolderBinding, relatedGamesTouch)

    }

    override fun getItemCount(): Int = relatedGames.size

    override fun onBindViewHolder(holder: RelatedGameViewHolder, position: Int) {

        holder.bind(relatedGames[position])

    }
}