package com.josegrillo.spookyvideogamesdatabase.view.utils

class ArrayListUtils {

    companion object {

        fun formatToWebService(idsArrayList: ArrayList<Long>?): String {

            var result = ""

            idsArrayList?.forEach {

                if (result.isEmpty()) {

                    result = it.toString()

                } else {

                    result += ",$it"

                }

            }


            return result
        }


    }

}