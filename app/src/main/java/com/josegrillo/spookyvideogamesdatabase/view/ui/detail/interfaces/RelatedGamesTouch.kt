package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.interfaces

import com.josegrillo.spookyvideogamesdatabase.view.model.Game

interface RelatedGamesTouch {

    fun onRelatedGameTouch(gameSelected: Game)

}