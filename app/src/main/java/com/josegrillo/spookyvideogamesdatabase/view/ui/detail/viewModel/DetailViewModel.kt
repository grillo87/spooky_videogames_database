package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewModel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.josegrillo.spookyvideogamesdatabase.domain.model.mapper.GameMapper
import com.josegrillo.spookyvideogamesdatabase.domain.model.mapper.ImageMapper
import com.josegrillo.spookyvideogamesdatabase.domain.usecase.GetGamesById
import com.josegrillo.spookyvideogamesdatabase.view.model.Game
import com.josegrillo.spookyvideogamesdatabase.view.model.Image
import com.josegrillo.spookyvideogamesdatabase.view.utils.ArrayListUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DetailViewModel @Inject constructor(val getGamesById: GetGamesById) : ViewModel() {

    val gameDetail = MutableLiveData<Game>()
    val loadingStatus = MutableLiveData<Boolean>()
    val playersPerspectivesList = MutableLiveData<String>()
    val developersList = MutableLiveData<String>()
    val platformsList = MutableLiveData<String>()
    val collectionId = MutableLiveData<String>()
    val screenshotsList = MutableLiveData<ArrayList<Image>>()
    val artWorksList = MutableLiveData<ArrayList<Image>>()
    val errorStatus = MutableLiveData<Throwable>()

    fun loadGameDetail(idsGame: String) {

        loadingStatus.value = true
        errorStatus.value = null

        getGamesById.getGamesById(idsGame)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ gamesResult ->

                    gamesResult?.let {

                        if (gamesResult.isNotEmpty()) {

                            gameDetail.value = GameMapper.gameApiResultToViewModel(it[0])
                            loadingStatus.value = false

                            it[0].playerPerspectives?.let {

                                playersPerspectivesList.value = ArrayListUtils.formatToWebService(it)

                            }

                            it[0].developers?.let {

                                developersList.value = ArrayListUtils.formatToWebService(it)

                            }

                            it[0].platforms?.let {

                                platformsList.value = ArrayListUtils.formatToWebService(it)

                            }


                            it[0].collection?.let {

                                collectionId.value = it.toString()

                            }

                            it[0].artworks?.let {

                                artWorksList.value = ImageMapper.imagesApiToImagesViewModel(it)

                            }


                            it[0].screenshots?.let {

                                screenshotsList.value = ImageMapper.imagesApiToImagesViewModel(it)

                            }


                        } else {

                            loadingStatus.value = false
                            errorStatus.value = Exception()

                        }

                    }

                },
                        { error ->

                            loadingStatus.value = false
                            errorStatus.value = error

                        }
                )


    }


}
