package com.josegrillo.spookyvideogamesdatabase.view.ui.splash

import android.content.Intent
import android.os.Bundle
import com.josegrillo.kotlinmvp.view.base.BaseActivity
import com.josegrillo.spookyvideogamesdatabase.R
import com.josegrillo.spookyvideogamesdatabase.view.contracts.SplashContract
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.activity.DetailActivity
import com.josegrillo.spookyvideogamesdatabase.view.ui.main.activity.MainActivity
import com.josegrillo.spookyvideogamesdatabase.view.utils.NavigatorUtils
import kotlinx.android.synthetic.main.activity_splash.*
import java.util.*

class SplashActivity : BaseActivity(), SplashContract {

    val splashTimeDuration: Long = 5000

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_splash)
        super.onCreate(savedInstanceState)

        startSplashTimer()

    }

    override fun setCustomFonts() {

        activitySplashTitleTextview?.typeface = customFont

    }

    override fun startSplashTimer() {

        val task = object : TimerTask() {
            override fun run() {

                navigateToMainList()

            }
        }

        val timer = Timer()
        timer.schedule(task, splashTimeDuration)

    }

    override fun navigateToMainList() {

        NavigatorUtils.navigateToMainActivity(this)

    }

}
