package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewModel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.josegrillo.spookyvideogamesdatabase.domain.model.mapper.GameMapper
import com.josegrillo.spookyvideogamesdatabase.domain.usecase.GetGameCollections
import com.josegrillo.spookyvideogamesdatabase.domain.usecase.GetGamesById
import com.josegrillo.spookyvideogamesdatabase.view.model.Game
import com.josegrillo.spookyvideogamesdatabase.view.utils.ArrayListUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RelatedGamesViewModel @Inject constructor(val getGameCollections: GetGameCollections,
                                                val getGamesById: GetGamesById) : ViewModel() {

    val relatedGamesList = MutableLiveData<ArrayList<Game>>()
    val relatedGamesLoadingStatus = MutableLiveData<Boolean>()
    val errorStatus = MutableLiveData<Throwable>()


    fun loadRelatedGames(idCollection: String?) {

        errorStatus.value = null

        idCollection?.let {

            getGameCollections.getGameCollections(it)
                    .flatMap { gameCollections ->

                        var gamesIds = ""
                        gameCollections[0].games?.let {

                            gamesIds = ArrayListUtils.formatToWebService(it)
                        }

                        return@flatMap getGamesById.getGamesById(gamesIds)

                    }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { relatedGames ->

                                relatedGames?.let {

                                    relatedGamesLoadingStatus.value = false


                                    if (it.isNotEmpty()) {

                                        relatedGamesList.value = GameMapper.gamesApiResultToGamesViewModel(it)

                                    }


                                }

                            },
                            { error ->
                                relatedGamesLoadingStatus.value = false
                                errorStatus.value = error
                            })


        }


    }


}