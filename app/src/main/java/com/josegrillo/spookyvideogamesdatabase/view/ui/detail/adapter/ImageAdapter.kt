package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.josegrillo.spookyvideogamesdatabase.R
import com.josegrillo.spookyvideogamesdatabase.databinding.DeveloperItemViewHolderBinding
import com.josegrillo.spookyvideogamesdatabase.databinding.ImageItemViewHolderBinding
import com.josegrillo.spookyvideogamesdatabase.databinding.PlayersPerspectiveItemViewHolderBinding
import com.josegrillo.spookyvideogamesdatabase.view.model.Developer
import com.josegrillo.spookyvideogamesdatabase.view.model.Image
import com.josegrillo.spookyvideogamesdatabase.view.model.PlayersPerspective
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.interfaces.DisplayDialogImageOnTouch
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewholder.DeveloperViewHolder
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewholder.ImageViewHolder
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewholder.PlayerPerspectiveViewHolder

class ImageAdapter(val images: ArrayList<Image>,
                   val displayDialogImageOnTouch: DisplayDialogImageOnTouch) : RecyclerView.Adapter<ImageViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val imageItemViewHolderBinding: ImageItemViewHolderBinding = DataBindingUtil.inflate(
                layoutInflater, R.layout.image_item_view_holder, parent, false)
        return ImageViewHolder(imageItemViewHolderBinding, displayDialogImageOnTouch)

    }

    override fun getItemCount(): Int = images.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {

        holder.bind(images[position])

    }
}