package com.josegrillo.spookyvideogamesdatabase.view.ui.main.fragment.list.adapter

import android.databinding.DataBindingUtil
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.josegrillo.spookyvideogamesdatabase.R
import com.josegrillo.spookyvideogamesdatabase.databinding.GameItemViewholderBinding
import com.josegrillo.spookyvideogamesdatabase.view.model.Game
import com.josegrillo.spookyvideogamesdatabase.view.ui.main.fragment.list.interfaces.GameSelectedTouch
import com.josegrillo.spookyvideogamesdatabase.view.ui.main.fragment.list.viewholder.GameItemViewHolder

class GameAdapter(val gamesList: ArrayList<Game>,
                  val gameSelectedTouch: GameSelectedTouch,
                  val customFont: Typeface?) : RecyclerView.Adapter<GameItemViewHolder>() {

    override fun getItemCount(): Int {

        return gamesList.size

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameItemViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val gameItemViewHolderBinding: GameItemViewholderBinding = DataBindingUtil.inflate(
                layoutInflater, R.layout.game_item_viewholder, parent, false)
        return GameItemViewHolder(gameItemViewHolderBinding, gameSelectedTouch, customFont)

    }

    override fun onBindViewHolder(holder: GameItemViewHolder, position: Int) {

        holder.bind(gamesList[position])


    }


    fun appendElements(gamesListAditional: ArrayList<Game>) {

        gamesList.addAll(gamesListAditional)
        this.notifyDataSetChanged()

    }

}