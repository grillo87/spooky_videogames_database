package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewModel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.josegrillo.spookyvideogamesdatabase.domain.model.mapper.GamePlatformMapper
import com.josegrillo.spookyvideogamesdatabase.domain.usecase.GetPlatformsById
import com.josegrillo.spookyvideogamesdatabase.view.model.GamePlatform
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PlatformViewModel @Inject constructor(val getPlatformsById: GetPlatformsById) : ViewModel() {

    val platformsList = MutableLiveData<ArrayList<GamePlatform>>()
    val platformsLoadingStatus = MutableLiveData<Boolean>()
    val errorStatus = MutableLiveData<Throwable>()


    fun loadGamePlatforms(idsPlatforms: String?) {

        errorStatus.value = null

        idsPlatforms?.let {

            getPlatformsById.getGamePlatforms(it)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { gamePlatforms ->

                                gamePlatforms?.let {

                                    platformsLoadingStatus.value = false


                                    if (it.isNotEmpty()) {

                                        platformsList.value = GamePlatformMapper.gamePlatformsApiToGamePlatformsViewModel(it)

                                    }


                                }

                            },
                            { error ->
                                platformsLoadingStatus.value = false
                                errorStatus.value = error
                            })


        }


    }


}