package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.josegrillo.kotlinmvp.di.component.DaggerActivitiesComponent
import com.josegrillo.spookyvideogamesdatabase.R
import com.josegrillo.spookyvideogamesdatabase.databinding.FragmentDevelopersBinding
import com.josegrillo.spookyvideogamesdatabase.di.module.FragmentsModule
import com.josegrillo.spookyvideogamesdatabase.view.base.BaseFragment
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.adapter.DeveloperAdapter
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.interfaces.RecyclerViewObserver
import com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewModel.DevelopersViewModel
import kotlinx.android.synthetic.main.fragment_developers.view.*
import javax.inject.Inject

private const val ARG_PARAM_DEVELOPERS_IDS = "developersIds"

class DevelopersFragment : BaseFragment(), RecyclerViewObserver {

    private var developersIds: String? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var fragmentDevelopersBinding: FragmentDevelopersBinding
    lateinit var developersViewModel: DevelopersViewModel

    override fun onCreate(savedInstanceState: Bundle?) {

        arguments?.let {
            developersIds = it.getString(ARG_PARAM_DEVELOPERS_IDS)
        }

        super.onCreate(savedInstanceState)


    }

    override fun injectDependency() {

        val developersComponent = DaggerActivitiesComponent.builder()
                .fragmentsModule(FragmentsModule())
                .build()

        developersComponent.inject(this)

    }

    override fun initializeDataBinding() {

        developersViewModel = ViewModelProviders.of(this, viewModelFactory)[DevelopersViewModel::class.java]

        developersIds?.let {

            developersViewModel.loadGameDevelopers(developersIds)

        }

    }

    override fun initializeObservers() {

        initializeRecyclerViewObserver()

    }

    override fun initializeRecyclerViewObserver() {

        developersViewModel.companiesList.observe(this, Observer { developers ->

            developers?.let {

                fragmentDevelopersBinding.root.fragmentDevelopersRecyclerView.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
                fragmentDevelopersBinding.root.fragmentDevelopersRecyclerView.adapter = DeveloperAdapter(it)


            }

        })


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        fragmentDevelopersBinding =
                DataBindingUtil.inflate(
                        inflater, R.layout.fragment_developers, container, false)
        fragmentDevelopersBinding.gamesDevelopersViewModel = developersViewModel
        fragmentDevelopersBinding.setLifecycleOwner(this)

        return fragmentDevelopersBinding.root
    }

    override fun initializeErrorListener() {

        developersViewModel.errorStatus.observe(this, Observer {


            it?.let {

                this.getParentActivity().displayErrorMessage(it)

            }


        })

    }


    companion object {

        @JvmStatic
        fun newInstance(developersIds: String) =
                DevelopersFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM_DEVELOPERS_IDS, developersIds)
                    }
                }
    }

}