package com.josegrillo.spookyvideogamesdatabase.view.contracts

interface DetailContract {

    fun listenGamePerspectiveObserver()

    fun listenDevelopersObserver()

    fun listenGamePlatformsObserver()

    fun listenCollectionObserver()

    fun listenScreenshotsObserver()

    fun listenArtworksObserver()

    fun loadBanner()

}