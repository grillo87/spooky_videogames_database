package com.josegrillo.spookyvideogamesdatabase.view.model

enum class GameRating constructor(val rating: Long) {
    RP(1), EC(2), E(3), E10(4), T(5), M(6), AO(7);

    companion object {
        fun from(findValue: Long): GameRating = GameRating.values().first { it.rating == findValue }
    }

}