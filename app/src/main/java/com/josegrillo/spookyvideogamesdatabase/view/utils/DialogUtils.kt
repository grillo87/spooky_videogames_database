package com.josegrillo.spookyvideogamesdatabase.view.utils

import android.app.Dialog
import android.graphics.Typeface
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import com.josegrillo.kotlinmvp.utils.GlideApp
import com.josegrillo.kotlinmvp.view.base.BaseActivity
import com.josegrillo.spookyvideogamesdatabase.R
import com.josegrillo.spookyvideogamesdatabase.view.model.Image

class DialogUtils {

    companion object {

        fun displayImageDialog(image: Image, activity: BaseActivity) {

            val dialog = Dialog(activity)
            dialog.setCancelable(true)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.dialog_image_display)

            val imageViewContainer = dialog.findViewById(R.id.dialogImageDisplayContainer) as ImageView
            imageViewContainer.setOnTouchListener { v, event ->
                dialog?.dismiss()
                return@setOnTouchListener true
            }

            GlideApp.with(activity).load(image.url).fitCenter().into(imageViewContainer)
            dialog.show()

        }

    }

}