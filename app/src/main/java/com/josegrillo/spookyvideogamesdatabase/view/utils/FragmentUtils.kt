package com.josegrillo.spookyvideogamesdatabase.view.utils

import android.support.v4.app.Fragment
import com.josegrillo.kotlinmvp.view.base.BaseActivity
import com.josegrillo.spookyvideogamesdatabase.R

class FragmentUtils {

    companion object {

        fun replaceFragment(resourceId: Int, activity: BaseActivity, fragment: Fragment) {

            val transaction = activity.supportFragmentManager.beginTransaction()
            transaction.replace(resourceId, fragment)
            transaction.commit()


        }

    }

}