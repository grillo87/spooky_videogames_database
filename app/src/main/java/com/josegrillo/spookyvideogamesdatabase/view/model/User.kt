package com.josegrillo.spookyvideogamesdatabase.view.model

data class User(val name: String? = null)