package com.josegrillo.spookyvideogamesdatabase.view.ui.main.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.view.MenuItem
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.josegrillo.kotlinmvp.view.base.BaseActivity
import com.josegrillo.spookyvideogamesdatabase.R
import com.josegrillo.spookyvideogamesdatabase.view.contracts.MainContract
import com.josegrillo.spookyvideogamesdatabase.view.ui.main.fragment.list.fragment.ListFragment
import com.josegrillo.spookyvideogamesdatabase.view.utils.FragmentUtils
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity(), MainContract {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_main)
        super.onCreate(savedInstanceState)

        loadBanner()
        navigateToInitialOption()

    }


    override fun initializeSupportActionBar() {

        setSupportActionBar(activityToolBar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_menu)

    }

    override fun initializeDrawerMenu() {

        activityMainNavigationView?.setNavigationItemSelectedListener { menuItem ->

            when (menuItem.itemId) {

                R.id.homeMenuOption -> return@setNavigationItemSelectedListener displayHomeView()
                R.id.gitlabMenuOption -> return@setNavigationItemSelectedListener navigateToGitlabRepository()
                R.id.rateUsMenuOption -> return@setNavigationItemSelectedListener navigateToGooglePlay()

            }

            false
        }


    }

    override fun navigateToInitialOption() {

        activityMainNavigationView?.setCheckedItem(R.id.homeMenuOption)
        activityMainNavigationView?.menu?.performIdentifierAction(R.id.homeMenuOption, 0)

    }

    override fun displayHomeView(): Boolean {

        supportActionBar?.title = resources.getString(R.string.homeOptionMenuText)
        activityMainDrawerLayout.closeDrawers()
        FragmentUtils.replaceFragment(R.id.activityMainFrameLayout, this, ListFragment())

        return true

    }

    override fun navigateToGitlabRepository(): Boolean {

        val gitlabIntent = Intent(Intent.ACTION_VIEW, Uri.parse(resources.getString(R.string.gitlab_url_repository)))
        startActivity(gitlabIntent)

        activityMainDrawerLayout.closeDrawers()
        return true

    }

    override fun navigateToGooglePlay(): Boolean {

        var googlePlayIntent: Intent? = null

        try {

            googlePlayIntent = Intent(Intent.ACTION_VIEW, Uri.parse(resources.getString(R.string.google_play_deep_linking)))

        } catch (anfe: android.content.ActivityNotFoundException) {

            googlePlayIntent = Intent(Intent.ACTION_VIEW, Uri.parse(resources.getString(R.string.google_play_url)))
        }

        startActivity(googlePlayIntent)

        activityMainDrawerLayout.closeDrawers()
        return true

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {

            android.R.id.home -> activityMainDrawerLayout?.openDrawer(GravityCompat.START)
        }

        return true
    }


    override fun loadBanner() {

        MobileAds.initialize(this, resources.getString(R.string.activityMainBannerUnitid))
        val adRequest = AdRequest.Builder().build()
        activityMainBannerAdview.loadAd(adRequest)

    }

}



