package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewholder

import android.support.v7.widget.RecyclerView
import com.josegrillo.spookyvideogamesdatabase.databinding.PlayersPerspectiveItemViewHolderBinding
import com.josegrillo.spookyvideogamesdatabase.view.model.PlayersPerspective

class PlayerPerspectiveViewHolder(val playersPerspectiveItemViewHolderBinding: PlayersPerspectiveItemViewHolderBinding) : RecyclerView.ViewHolder(playersPerspectiveItemViewHolderBinding.root) {


    fun bind(playerPerspective: PlayersPerspective) {

        playersPerspectiveItemViewHolderBinding.playerPerspective = playerPerspective
        playersPerspectiveItemViewHolderBinding.executePendingBindings()

    }


}