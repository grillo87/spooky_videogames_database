package com.josegrillo.spookyvideogamesdatabase.view.utils

import android.content.Context
import com.josegrillo.spookyvideogamesdatabase.R
import retrofit2.HttpException
import java.net.ConnectException
import java.util.concurrent.TimeoutException

class ErrorUtils {

    companion object {

        fun errorHandler(error: Throwable, context: Context): String {

            var result = ""

            if ((error is TimeoutException) || (error is ConnectException) || (error is HttpException)) {

                result = context.resources.getString(R.string.conectionErrorMessage)

            }  else {

                result = context.resources.getString(R.string.unexpectedErrorMessage)


            }


            return result

        }


    }

}