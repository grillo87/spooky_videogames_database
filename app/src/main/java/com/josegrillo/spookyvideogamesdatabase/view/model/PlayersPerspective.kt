package com.josegrillo.spookyvideogamesdatabase.view.model

import android.databinding.BaseObservable

data class PlayersPerspective(val name: String) : BaseObservable()