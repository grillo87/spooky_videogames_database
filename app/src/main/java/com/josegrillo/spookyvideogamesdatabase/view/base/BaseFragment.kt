package com.josegrillo.spookyvideogamesdatabase.view.base

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import com.josegrillo.kotlinmvp.view.base.BaseActivity

open class BaseFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        injectDependency()
        defineViewModel()
        initializeDataBinding()
        setCustomFonts()
        initializeOnClickListeners()
        initializeObservers()

    }

    fun getParentActivity(): BaseActivity {

        return this.activity as BaseActivity

    }

    open fun setCustomFonts() {}

    open fun injectDependency() {}

    open fun defineViewModel() {}

    open fun initializeOnClickListeners() {}

    open fun initializeObservers() {}

    open fun initializeDataBinding() {}

    open fun initializeErrorListener() {}


}