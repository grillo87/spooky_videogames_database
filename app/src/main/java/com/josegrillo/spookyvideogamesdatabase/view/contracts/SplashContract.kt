package com.josegrillo.spookyvideogamesdatabase.view.contracts

interface SplashContract {

    fun startSplashTimer()

    fun navigateToMainList()

}