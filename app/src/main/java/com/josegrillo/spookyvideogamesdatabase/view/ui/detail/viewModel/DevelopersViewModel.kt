package com.josegrillo.spookyvideogamesdatabase.view.ui.detail.viewModel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.josegrillo.spookyvideogamesdatabase.domain.model.mapper.DeveloperMapper
import com.josegrillo.spookyvideogamesdatabase.domain.usecase.GetCompaniesById
import com.josegrillo.spookyvideogamesdatabase.view.model.Developer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DevelopersViewModel @Inject constructor(val getCompaniesById: GetCompaniesById) : ViewModel() {

    val companiesList = MutableLiveData<ArrayList<Developer>>()
    val companiesLoadingStatus = MutableLiveData<Boolean>()
    val errorStatus = MutableLiveData<Throwable>()


    fun loadGameDevelopers(idsCompanies: String?) {

        errorStatus.value = null

        idsCompanies?.let {

            getCompaniesById.getCompaniesById(it)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { playersPerspectives ->

                                playersPerspectives?.let {

                                    companiesLoadingStatus.value = false


                                    if (it.isNotEmpty()) {

                                        companiesList.value = DeveloperMapper.developersApiToDevelopersViewModel(it)

                                    }


                                }

                            },
                            { error ->

                                companiesLoadingStatus.value = false
                                errorStatus.value = error

                            })


        }


    }


}