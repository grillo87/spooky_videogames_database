package com.josegrillo.spookyvideogamesdatabase.utils

import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.robolectric.RuntimeEnvironment
import java.io.InputStream

class TestUtils {

    companion object {


        fun queueResponse(mockWebServer: MockWebServer, block: MockResponse.() -> Unit) {
            mockWebServer.enqueue(MockResponse().apply(block))
        }


        fun getResponseFromJson(fileName: String): String {

            try {
                val inputStream: InputStream = RuntimeEnvironment.application.applicationContext.assets.open("api-response/$fileName.json")
                val inputString = inputStream.bufferedReader().use {
                    it.readText()
                }
                return inputString
            } catch (e: Exception) {
                return ""
            }
        }


    }

}