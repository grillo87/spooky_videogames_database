package com.josegrillo.spookyvideogamesdatabase.stringUtilsTest

import com.josegrillo.spookyvideogamesdatabase.view.utils.StringUtils
import org.junit.Assert
import org.junit.Test

class StringUtilsFunsTest {

    @Test
    fun testNormalCoverUrl() {


        val demoUrl = "//images.igdb.com/igdb/image/upload/t_thumb/exlxirelwzpoqftj8phf.jpg"
        val result = StringUtils.formatImageUrl(demoUrl, StringUtils.COVER_BIG_VALUE)
        Assert.assertEquals("http://images.igdb.com/igdb/image/upload/t_cover_big/exlxirelwzpoqftj8phf.jpg", result)

    }


    @Test
    fun testNullCoverUrl() {

        var demoUrl: String? = null
        val result = StringUtils.formatImageUrl(demoUrl, StringUtils.COVER_BIG_VALUE)
        Assert.assertEquals("http:", result)

    }


    @Test
    fun testNormalDateFormat() {

        val demoDate = 1223942400000L
        val result = StringUtils.formatDateGame(demoDate, StringUtils.DATE_FORMAT)
        Assert.assertEquals("Released on Oct 13, 2008", result)

    }


    @Test
    fun testTimeToBeatDateFormat() {

        val demoTime = 45000L
        val result = StringUtils.timeToBeatFormat(demoTime)
        Assert.assertEquals("12 hrs 30 min", result)

    }


    @Test
    fun testNullDateFormat() {

        val demoDate: Long? = null
        val result = StringUtils.formatDateGame(demoDate, StringUtils.DATE_FORMAT)
        Assert.assertEquals("Not yet released", result)

    }

}