package com.josegrillo.spookyvideogamesdatabase.apiTests

import com.josegrillo.spookyvideogamesdatabase.domain.model.api.request.RequestFields
import com.josegrillo.spookyvideogamesdatabase.utils.TestUtils
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class GameSearchTest: BaseApiTest() {


    @Test
    fun gameSearch() {

        TestUtils.queueResponse(mockWebServer) {
            setResponseCode(200)
            setBody(TestUtils.getResponseFromJson("gameSearch"))
        }


        appApi
                .searchGame("dead space",
                        RequestFields.DEFAULT_FIELDS_VALUES,
                        RequestFields.DEFAULT_SCROLL_VALUE,
                        RequestFields.DEFAULT_LIMIT_VALUE)
                .test()
                .run {

                    values().let {

                        assertNoErrors()
                        assertValueCount(1)
                        Assert.assertEquals(it[0][0].id, 37L)
                        Assert.assertNotNull(it[0][0].name)
                        Assert.assertEquals(it[0][1].name, "Dead Space 2")
                        Assert.assertEquals(it[0][2].totalRating, 74.05803530288665)
                        Assert.assertEquals(it[0][3].games?.get(3), 7351L)
                        Assert.assertEquals(it[0][4].platforms?.get(1), 9L)
                        Assert.assertEquals(it[0][5].cover?.url, "//images.igdb.com/igdb/image/upload/t_thumb/dddxzgmu2rcbaawdq04p.jpg")
                        Assert.assertEquals(it[0][6].screenshots?.get(1)?.url, "//images.igdb.com/igdb/image/upload/t_thumb/xz4e7ws8wpgmvoewlvpy.jpg")


                    }
                }

    }

}