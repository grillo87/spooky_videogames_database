package com.josegrillo.spookyvideogamesdatabase.apiTests

import com.josegrillo.spookyvideogamesdatabase.utils.TestUtils
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class GameByIdTest : BaseApiTest() {

    @Test
    fun gameById() {

        TestUtils.queueResponse(mockWebServer) {
            setResponseCode(200)
            setBody(TestUtils.getResponseFromJson("gameId"))
        }


        appApi
                .gameById("1276")
                .test()
                .run {

                    values().let {

                        assertNoErrors()
                        assertValueCount(1)
                        Assert.assertEquals(it[0][0].id, 1276L)
                        Assert.assertNotNull(it[0][0].name)
                        Assert.assertEquals(it[0][0].name, "Deadly Premonition")
                        Assert.assertEquals(it[0][0].totalRating, 82.91115976018006)
                        Assert.assertEquals(it[0][0].games?.size, 10)
                        Assert.assertEquals(it[0][0].platforms?.get(1), 12L)
                        Assert.assertEquals(it[0][0].screenshots?.get(2)?.url, "//images.igdb.com/igdb/image/upload/t_thumb/wtobld2kyi6n2ajhvoxo.jpg")
                        Assert.assertEquals(it[0][0].cover?.url, "//images.igdb.com/igdb/image/upload/t_thumb/pdqn3dlyhch4lss2ixed.jpg")


                    }
                }


    }

}