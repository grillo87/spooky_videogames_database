package com.josegrillo.spookyvideogamesdatabase.apiTests

import com.josegrillo.spookyvideogamesdatabase.domain.model.api.request.RequestFields
import com.josegrillo.spookyvideogamesdatabase.utils.TestUtils
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class CompaniesByIdTest : BaseApiTest() {


    @Test
    fun companiesById() {


        TestUtils.queueResponse(mockWebServer) {
            setResponseCode(200)
            setBody(TestUtils.getResponseFromJson("companyById"))
        }


        appApi
                .companiesById("1",
                        RequestFields.NAME_AND_LOGO_FIELDS_VALUES)
                .test()
                .run {

                    values().let {

                        assertNoErrors()
                        assertValueCount(1)
                        Assert.assertEquals(it[0][0].id, 1L)
                        Assert.assertNotNull(it[0][0].name)
                        Assert.assertEquals(it[0][0].name, "Electronic Arts")
                        Assert.assertEquals(it[0][0].logo?.url, "//images.igdb.com/igdb/image/upload/t_thumb/i9s3h5sqtvzisfwik1za.jpg")

                    }
                }


    }

}