package com.josegrillo.spookyvideogamesdatabase.apiTests

import com.josegrillo.spookyvideogamesdatabase.domain.model.api.request.RequestFields
import com.josegrillo.spookyvideogamesdatabase.utils.TestUtils
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class PlatformTest : BaseApiTest() {

    @Test
    fun platformsById() {


        TestUtils.queueResponse(mockWebServer) {
            setResponseCode(200)
            setBody(TestUtils.getResponseFromJson("platform"))
        }


        appApi
                .platforms("12",
                        RequestFields.NAME_FIELD_VALUE)
                .test()
                .run {

                    values().let {

                        assertNoErrors()
                        assertValueCount(1)
                        Assert.assertEquals(it[0][0].id, 12L)
                        Assert.assertNotNull(it[0][0].name)
                        Assert.assertEquals(it[0][0].name, "Xbox 360")

                    }
                }


    }

}