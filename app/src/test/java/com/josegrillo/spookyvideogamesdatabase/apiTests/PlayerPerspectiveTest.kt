package com.josegrillo.spookyvideogamesdatabase.apiTests

import com.josegrillo.spookyvideogamesdatabase.domain.model.api.request.RequestFields
import com.josegrillo.spookyvideogamesdatabase.utils.TestUtils
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class PlayerPerspectiveTest : BaseApiTest() {

    @Test
    fun playersPerspective() {


        TestUtils.queueResponse(mockWebServer) {
            setResponseCode(200)
            setBody(TestUtils.getResponseFromJson("playerPerspective"))
        }


        appApi
                .playersPerspective("1,2",
                        RequestFields.NAME_FIELD_VALUE)
                .test()
                .run {

                    values().let {

                        assertNoErrors()
                        assertValueCount(1)
                        Assert.assertEquals(it[0][0].id, 1L)
                        Assert.assertNotNull(it[0][0].name)
                        Assert.assertEquals(it[0][1].name, "Third person")

                    }
                }


    }

}