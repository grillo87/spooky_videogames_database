package com.josegrillo.spookyvideogamesdatabase.apiTests

import com.josegrillo.spookyvideogamesdatabase.domain.model.api.request.RequestFields
import com.josegrillo.spookyvideogamesdatabase.utils.TestUtils
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner


@RunWith(RobolectricTestRunner::class)
class GameListTest : BaseApiTest() {

    @Test
    fun getGameList() {

        TestUtils.queueResponse(mockWebServer) {
            setResponseCode(200)
            setBody(TestUtils.getResponseFromJson("gameList"))
        }

        appApi
                .gamesList(RequestFields.DEFAULT_FIELDS_VALUES,
                        RequestFields.DEFAULT_LIMIT_VALUE,
                        RequestFields.DEFAULT_SCROLL_VALUE,
                        RequestFields.DEFAUL_GENRE_VALUE,
                        RequestFields.DEFAULT_THEME_VALUE,
                        RequestFields.DEFAULT_POPULARITY_VALUE,
                        RequestFields.DEFAULT_NAME_VALUE)
                .test()
                .run {

                    values().let {

                        assertNoErrors()
                        assertValueCount(1)
                        Assert.assertEquals(it[0].size, 10)
                        Assert.assertEquals(it[0][0].id, 24958L)
                        Assert.assertEquals(it[0][1].name, "10 Years After")
                        Assert.assertEquals(it[0][2].timeToBeat?.normally, 7200L)
                        Assert.assertEquals(it[0][3].playerPerspectives?.get(0), 4L)
                        Assert.assertEquals(it[0][4].releaseDate, 1476057600000)
                        Assert.assertEquals(it[0][5].screenshots?.get(2)?.url, "//images.igdb.com/igdb/image/upload/t_thumb/qt25cmllv1emzly1aoqy.jpg")
                        Assert.assertEquals(it[0][6].totalRating, 53.6256830601093)
                        Assert.assertEquals(it[0][7].cover?.url, "//images.igdb.com/igdb/image/upload/t_thumb/fon2humdruxczxpvzucg.jpg")
                        Assert.assertEquals(it[0][8].developers?.get(0), 10011L)
                        Assert.assertEquals(it[0][9].esrb, null)


                    }
                }

    }

}