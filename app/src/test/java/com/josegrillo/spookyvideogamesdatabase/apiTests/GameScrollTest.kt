package com.josegrillo.spookyvideogamesdatabase.apiTests

import com.josegrillo.spookyvideogamesdatabase.utils.TestUtils
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class GameScrollTest: BaseApiTest() {


    @Test
    fun gameScroll() {

        TestUtils.queueResponse(mockWebServer) {
            setResponseCode(200)
            setBody(TestUtils.getResponseFromJson("gameScroll"))
        }


        appApi
                .gameScroll("games/scroll/DXF1ZXJ5QW5kRmV0Y2gBAAAAAAAHMOUWRVdqWW9YQnJUYVNBNDNYV0FWNlItUQ==/?fields=name,summary,storyline,collection,total_rating,developers,time_to_beat,player_perspectives,status,first_release_date,screenshots,cover,esrb,artworks")
                .test()
                .run {

                    values().let {

                        assertNoErrors()
                        assertValueCount(1)
                        Assert.assertEquals(it[0].size, 10)
                        Assert.assertEquals(it[0][0].id, 37195L)
                        Assert.assertEquals(it[0][1].name, "6 Days a Sacrifice")
                        Assert.assertNull(it[0][2].timeToBeat)
                        Assert.assertEquals(it[0][3].playerPerspectives?.size, 3)
                        Assert.assertEquals(it[0][5].screenshots?.get(0)?.url, "//images.igdb.com/igdb/image/upload/t_thumb/svxvjocxb1wscbxetccx.jpg")
                        Assert.assertEquals(it[0][6].releaseDate, 1404432000000)
                        Assert.assertEquals(it[0][7].cover?.url, null)
                        Assert.assertEquals(it[0][9].developers?.get(0), 10892L)
                        Assert.assertEquals(it[0][9].esrb?.rating, 6L)

                    }
                }


    }

}